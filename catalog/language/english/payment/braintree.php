<?php
//==============================================================================
// Braintree Payment Gateway v210.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================

// Text
$_['text_pay_with_paypal']			= 'Pay With PayPal';

$_['text_pay_with_card']			= 'Pay With a Credit Card';

$_['text_ending_in']				= 'ending in';
$_['text_delete_this_card']			= 'Delete this card';
$_['text_use_a_new_card']			= '--- Use a New Card ---';

$_['text_card_name']				= 'Cardholder Name:';
$_['text_card_number']				= 'Card Number:';
$_['text_card_type']				= 'Card Type:';
$_['text_card_expiry']				= 'Card Expiry (MM/YY):';
$_['text_card_security']			= 'Card Security Code (CVV):';
$_['text_store_card']				= 'Store Card For Future Use:';

$_['text_loading']					= 'Loading...';
$_['text_confirm']					= 'This operation cannot be done. Continue?';
$_['text_please_wait']				= 'Please wait...';
$_['text_success']					= 'Success!';
$_['text_to_be_charged_separately']	= 'To Be Charged Separately';

// Error
$_['error_error']					= 'Error:\n';
$_['error_card_token_not_found']	= 'Error: Card token not found';
$_['error_customer_required']		= 'Error: You must create a customer account to purchase a subscription product.';
?>