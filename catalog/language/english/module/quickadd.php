<?php
// Heading
$_['heading_title'] = 'Quick Shop	';
$_['text_product']='Product';
$_['text_qty']='Qty';
$_['text_added_item']='Item Added';
$_['text_items']    = '%s item(s) - %s';
$_['text_empty']    = 'Your shopping cart is empty!';
$_['text_cart']     = 'View Cart';
$_['text_checkout'] = 'Checkout';
$_['error_please_select_product']='Please select Product';
$_['text_msg_options']='You must specify options to buy this product';