<?php
//==============================================================================
// Braintree Payment Gateway v210.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================

class ControllerPaymentBraintree extends Controller {
	private $type = 'payment';
	private $name = 'braintree';
	
	public function index() {
		$data['type'] = $this->type;
		$data['name'] = $this->name;
		
		$data['settings'] = $this->getSettings();
		$data['language'] = $this->session->data['language'];
		$data['logged_in'] = $this->customer->isLogged();
		
		$data = array_merge($data, $this->load->language($this->type . '/' . $this->name));
		
		$this->load->model('checkout/order');
		$data['order_info'] = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		
		$this->loadBraintree();
		$braintree_customer = $this->getCustomerCards();
		
		if ($braintree_customer != 'not_found') {
			$data['stored_cards'] = ($this->config->get('braintree_allow_stored_cards')) ? $braintree_customer : array();
			$data['client_token'] = Braintree_ClientToken::generate(array('customerId' => 'customer_' . $this->customer->getId()));
		} else {
			$data['stored_cards'] = array();
			$data['client_token'] = Braintree_ClientToken::generate();
		}
		
		$template = (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $this->type . '/' . $this->name . '.tpl')) ? $this->config->get('config_template') : 'default';
		$template_file = $template . '/template/' . $this->type . '/' . $this->name . '.tpl';
		
		if (version_compare(VERSION, '2.0') < 0) {
			$this->data = $data;
			$this->template = $template_file;
			$this->render();
		} else {
			return $this->load->view($template_file, $data);
		}
	}
	
	//==============================================================================
	// Private functions
	//==============================================================================
	private function getSettings() {
		$settings = array();
		$settings_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `" . (version_compare(VERSION, '2.0.1') < 0 ? 'group' : 'code') . "` = '" . $this->db->escape($this->name) . "' ORDER BY `key` ASC");
		
		foreach ($settings_query->rows as $setting) {
			$value = $setting['value'];
			if ($setting['serialized']) {
				$value = (version_compare(VERSION, '2.1', '<')) ? unserialize($setting['value']) : json_decode($setting['value']);
			}
			$split_key = preg_split('/_(\d+)_?/', str_replace($this->name . '_', '', $setting['key']), -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
			
				if (count($split_key) == 1)	$settings[$split_key[0]] = $value;
			elseif (count($split_key) == 2)	$settings[$split_key[0]][$split_key[1]] = $value;
			elseif (count($split_key) == 3)	$settings[$split_key[0]][$split_key[1]][$split_key[2]] = $value;
			elseif (count($split_key) == 4)	$settings[$split_key[0]][$split_key[1]][$split_key[2]][$split_key[3]] = $value;
			else 							$settings[$split_key[0]][$split_key[1]][$split_key[2]][$split_key[3]][$split_key[4]] = $value;
		}
		
		return $settings;
	}
	
	private function loadBraintree() {
		$settings = $this->getSettings();
		require_once(DIR_SYSTEM . 'library/braintree/Braintree.php');
		Braintree_Configuration::environment($settings['server_mode']);
		Braintree_Configuration::merchantId($settings[$settings['server_mode'] . '_merchant_id']);
		Braintree_Configuration::publicKey($settings[$settings['server_mode'] . '_public_key']);
		Braintree_Configuration::privateKey($settings[$settings['server_mode'] . '_private_key']);
	}
	
	private function getCustomerCards() {
		$this->loadBraintree();
		$stored_cards = array();
		$data = $this->load->language($this->type . '/' . $this->name);
		
		try {
			$customer = Braintree_Customer::find('customer_' . $this->customer->getId());
			foreach ($customer->creditCards as $cc) {
				$stored_cards[] = array(
					'token'			=> $cc->token,
					'name'			=> $cc->cardType . ' ' . $data['text_ending_in'] . ' ' . $cc->last4 . ' (' . $cc->expirationMonth . '/' . substr($cc->expirationYear, 2, 4) . ')',
					'is_default'	=> $cc->isDefault(),
					'type'			=> $cc->cardType
				);
			}
		} catch (Exception $e) {
			return 'not_found';
		}
		
		return $stored_cards;
	}
	
	//==============================================================================
	// Public functions
	//==============================================================================
	public function deleteCard() {
		$this->loadBraintree();
		try {
			$result = Braintree_PaymentMethod::find($this->request->get['token']);
			Braintree_PaymentMethod::delete($this->request->get['token']);
			Braintree_Address::delete($result->billingAddress->customerId, $result->billingAddress->id);
			echo true;
		} catch (Exception $e) {
			echo false;
		}
	}
	
	public function send() {
		$this->loadBraintree();
		$settings = $this->getSettings();
		
		$data = array();
		$data = array_merge($data, $this->load->language('total/total'));
		$data = array_merge($data, $this->load->language($this->type . '/' . $this->name));
		
		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		
		// Check for subscription products
		$revised_total = $order_info['total'];
		$subscriptions = array();
		
		if (!empty($settings['subscriptions'])) {
			$plans = Braintree_Plan::all();
			foreach ($this->cart->getProducts() as $product) {
				$product_query = $this->db->query("SELECT location FROM " . DB_PREFIX . "product WHERE product_id = " . (int)$product['product_id']);
				if (empty($product_query->row['location'])) continue;
				
				foreach ($plans as $plan) {
					if ($product_query->row['location'] == $plan->id) {
						if ($settings['prevent_guests'] && !$this->customer->getId()) {
							$this->response->setOutput(json_encode(array('error' => $data['error_customer_required'])));
							return;
						}
						$revised_total -= $product['total'];
						$subscriptions[] = array(
							'cost'			=> $product['total'],
							'id'			=> $plan->id,
							'key'			=> $product['key'],
							'name'			=> $plan->name,
							'product'		=> $product['name'],
							'quantity'		=> $product['quantity'],
							'tax_class_id'	=> $product['tax_class_id'],
							'separate'		=> (!empty($plan->billingDayOfMonth) || !empty($plan->trialDuration))
						);
					}
				}
			}
		}
		
		// Add in shipping and tax costs for subscription products
		if ($settings['subscription_shipping']) {
			$country_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE country_id = " . (int)$order_info['shipping_country_id']);
			$shipping_address = array(
				'firstname'		=> $order_info['shipping_firstname'],
				'lastname'		=> $order_info['shipping_lastname'],
				'company'		=> $order_info['shipping_company'],
				'address_1'		=> $order_info['shipping_address_1'],
				'address_2'		=> $order_info['shipping_address_2'],
				'city'			=> $order_info['shipping_city'],
				'postcode'		=> $order_info['shipping_postcode'],
				'zone'			=> $order_info['shipping_zone'],
				'zone_id'		=> $order_info['shipping_zone_id'],
				'country'		=> $order_info['shipping_country'],
				'country_id'	=> $order_info['shipping_country_id'],
				'iso_code_2'	=> $order_info['shipping_iso_code_2'],
			);
			$session_cart = $this->session->data['cart'];
			
			foreach ($subscriptions as &$subscription) {
				foreach ($this->session->data['cart'] as $key => $quantity) {
					if ($key != $subscription['key']) {
						$this->cart->remove($key);
					}
				}
				
				$subscription['cost'] = $this->tax->calculate($subscription['cost'], $subscription['tax_class_id']);
				
				if (!$this->cart->hasShipping()) continue;
				
				$shipping_methods = array();
				$this->load->model('setting/extension');
				$shipping_methods = $this->model_setting_extension->getExtensions('shipping');
				
				foreach ($shipping_methods as $shipping_method) {
					if (!$this->config->get($shipping_method['code'] . '_status')) continue;
					
					$this->load->model('shipping/' . $shipping_method['code']);
					$quote = $this->{'model_shipping_' . $shipping_method['code']}->getQuote($shipping_address);
					if (empty($quote)) continue;
					
					foreach ($quote['quote'] as $q) {
						if ($q['title'] == $order_info['shipping_method']) {
							$subscription['cost'] += $this->tax->calculate($q['cost'], $q['tax_class_id']);
							break;
						}
					}
				}
				
				$this->session->data['cart'] = $session_cart;
			}
		}
		
		// Set up address arrays
		$billing_address = array(
			'firstName'			=> html_entity_decode($order_info['payment_firstname'], ENT_QUOTES, 'UTF-8'),
			'lastName'			=> html_entity_decode($order_info['payment_lastname'], ENT_QUOTES, 'UTF-8'),
			'company'			=> html_entity_decode($order_info['payment_company'], ENT_QUOTES, 'UTF-8'),
			'streetAddress' 	=> html_entity_decode($order_info['payment_address_1'], ENT_QUOTES, 'UTF-8'),
			'extendedAddress'	=> html_entity_decode($order_info['payment_address_2'], ENT_QUOTES, 'UTF-8'),
			'locality'			=> html_entity_decode($order_info['payment_city'], ENT_QUOTES, 'UTF-8'),
			'region'			=> html_entity_decode($order_info['payment_zone_code'], ENT_QUOTES, 'UTF-8'),
			'postalCode'		=> html_entity_decode($order_info['payment_postcode'], ENT_QUOTES, 'UTF-8'),
			'countryCodeAlpha2'	=> html_entity_decode($order_info['payment_iso_code_2'], ENT_QUOTES, 'UTF-8')
		);
		
		$shipping_address = array(
			'firstName'			=> html_entity_decode($order_info['shipping_firstname'], ENT_QUOTES, 'UTF-8'),
			'lastName'			=> html_entity_decode($order_info['shipping_lastname'], ENT_QUOTES, 'UTF-8'),
			'company'			=> html_entity_decode($order_info['shipping_company'], ENT_QUOTES, 'UTF-8'),
			'streetAddress' 	=> html_entity_decode($order_info['shipping_address_1'], ENT_QUOTES, 'UTF-8'),
			'extendedAddress'	=> html_entity_decode($order_info['shipping_address_2'], ENT_QUOTES, 'UTF-8'),
			'locality'			=> html_entity_decode($order_info['shipping_city'], ENT_QUOTES, 'UTF-8'),
			'region'			=> html_entity_decode($order_info['shipping_zone_code'], ENT_QUOTES, 'UTF-8'),
			'postalCode'		=> html_entity_decode($order_info['shipping_postcode'], ENT_QUOTES, 'UTF-8'),
			'countryCodeAlpha2'	=> html_entity_decode($order_info['shipping_iso_code_2'], ENT_QUOTES, 'UTF-8')
		);
		
		// Find or create customer, and update stored card address
		$old_customer = '';
		$new_customer = '';
		$card_nonce = $this->request->post['nonce'];
		$card_token = ($this->request->post['token'] == 'dropin') ? '' : $this->request->post['token'];
		$dropin_ui = ($this->request->post['token'] == 'dropin');
		
		if (!empty($card_token)) {
			$old_customer = 'customer_' . $this->customer->getId();
			$result = Braintree_PaymentMethod::update($card_token, array(
				'billingAddress' => array_merge($billing_address, array(
					'options'	=> array('updateExisting' => true)
				)),
				'options' => array(
					'makeDefault' => true,
				)
			));
			if (!$result->success) {
				$this->response->setOutput(json_encode(array('error' => $result->message)));
				return;
			}
		} else {
			try {
				$customer = Braintree_Customer::find('customer_' . $this->customer->getId());
				$old_customer = $customer->id;
				if ($customer->email != $order_info['email']) {
					$update_result = Braintree_Customer::update($old_customer, array('email' => $order_info['email']));
					if (!$update_result->success) {
						$this->log->write(strtoupper($this->name) . ' CUSTOMER UPDATE ERROR: ' . $update_result->message);
					}
				}
			} catch (Exception $e) {
				$new_customer = ($this->customer->isLogged()) ? 'customer_' . $this->customer->getId() : 'guest_' . $order_info['order_id'];
			}
		}
		
		$customer = array(
			'id'				=> $new_customer,
			'firstName'			=> html_entity_decode($order_info['firstname'], ENT_QUOTES, 'UTF-8'),
			'lastName'			=> html_entity_decode($order_info['lastname'], ENT_QUOTES, 'UTF-8'),
			'company'			=> html_entity_decode($order_info['payment_company'], ENT_QUOTES, 'UTF-8'),
			'phone'				=> $order_info['telephone'],
			'fax'				=> $order_info['fax'],
			'email'				=> $order_info['email']
		);
		
		// Charge card
		if ($revised_total > 0) {
			$store_card = (!empty($subscriptions) || $settings['store_card'] == 'always' || ($settings['store_card'] == 'choice' && !empty($this->request->post['store_card']) && $this->request->post['store_card'] == 'true'));
			
			$transaction_array = array(
				'type'					=> 'sale',
				'channel'				=> 'ClearThinking_BraintreePaymentGateway',
				'orderId'				=> $order_info['order_id'],
				'customerId'			=> $old_customer,
				'paymentMethodNonce'	=> $card_nonce,
				'paymentMethodToken'	=> $card_token,
				'customer'				=> $customer,
				'shipping'				=> $shipping_address,
				'options'	=> array(
					'submitForSettlement'				=> ($settings['charge_mode'] == 'submit') ? true : false,
					'storeInVault'						=> ($store_card) ? true : false,
					'addBillingAddressToPaymentMethod'	=> ($store_card && $settings['store_billing']) ? true : false,
					'storeShippingAddressInVault'		=> ($store_card && $settings['store_shipping']) ? true : false
				),
			);
			if (!$card_token && !$dropin_ui) {
				$transaction_array['billing'] = $billing_address;
			}
			if (!empty($this->request->post['device_data'])) {
				$transaction_array['deviceData'] = $this->request->post['device_data'];
			}
			
			if (!empty($settings[$this->session->data['currency'] . '_' . $settings['server_mode'] . '_merchant_account'])) {
				$transaction_array['merchantAccountId'] = $settings[$this->session->data['currency'] . '_' . $settings['server_mode'] . '_merchant_account'];
				$transaction_currency = $this->session->data['currency'];
			} else {
				$transaction_currency = $this->config->get('config_currency');
			}
			
			$default_currency = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `key` = 'config_currency' AND store_id = 0")->row;
			$transaction_array['amount'] = number_format(round($this->currency->convert($revised_total, $default_currency['value'], $transaction_currency), $this->currency->getDecimalPlace()), 2, '.', '');
			
			$result = Braintree_Transaction::sale($transaction_array);
			
			if (!$result->success) {
				$result->message = str_replace('payment_method_nonce does not contain a valid payment instrument type.', '', $result->message);
				$result->message = str_replace('Credit card must include number, payment_method_nonce, or venmo_sdk_payment_method_code.', '', $result->message);
				$this->response->setOutput(json_encode(array('error' => $result->message)));
				return;
			} else {
				$transaction = $result->transaction;
				$card_token = $transaction->creditCardDetails->token;
			}
		} elseif (!$card_token) {
			if ($old_customer) {
				$result = Braintree_PaymentMethod::create(array(
					'customer_id'			=> $old_customer,
					'paymentMethodNonce'	=> $card_nonce,
					'billingAddress'		=> $billing_address,
					'options'				=> array(
						'makeDefault'					=> true,
						'failOnDuplicatePaymentMethod'	=> true
					)
				));
				if (!$result->success) {
					$this->response->setOutput(json_encode(array('error' => $result->message)));
					return;
				} else {
					$card_token = $result->creditCard->token;
				}
			} else {
				$customer['paymentMethodNonce'] = $card_nonce;
				$result = Braintree_Customer::create($customer);
				if (!$result->success) {
					$this->response->setOutput(json_encode(array('error' => $result->message)));
					return;
				} else {
					$card_token = $result->customer->creditCards[0]->token;
				}
			}
		}
		
		// Subscribe customer to plans
		$plans_subscribed = array();
		
		foreach ($subscriptions as $subscription) {
			$result = Braintree_Subscription::create(array(
				'paymentMethodToken'	=> $card_token,
				'planId'				=> $subscription['id'],
				'price'					=> $subscription['cost']
			));
			
			if (!$result->success) {
				$this->response->setOutput(json_encode(array('error' => $result->message)));
				return;
			} else {
				$plans_subscribed[] = $result;
				if (empty($result->subscription->transactions) && $subscription['separate']) {
					$this->db->query("UPDATE `" . DB_PREFIX . "order` SET total = " . (float)$revised_total . " WHERE order_id = " . (int)$order_info['order_id']);
					if (version_compare(VERSION, '2.0') < 0) {
						$this->db->query("UPDATE " . DB_PREFIX . "order_total SET text = '" . $this->db->escape($this->currency->format($revised_total, $order_info[isset($order_info['currency']) ? 'currency' : 'currency_code'])) . "', value = " . (float)$revised_total . " WHERE order_id = " . (int)$order_info['order_id'] . " AND title = '" . $this->db->escape($data['text_total']) . "'");
						$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = " . (int)$order_info['order_id'] . ", code = 'total', title = '" . $this->db->escape($data['text_to_be_charged_separately']) . " (" . $subscription['product'] . ")', text = '" . $this->db->escape($this->currency->format(-$subscription['cost'], $order_info[isset($order_info['currency']) ? 'currency' : 'currency_code'])) . "', value = " . (float)-$subscription['cost'] . ", sort_order = " . ((int)$this->config->get('total_sort_order')-1));
					} else {
						$this->db->query("UPDATE " . DB_PREFIX . "order_total SET value = " . (float)$revised_total . " WHERE order_id = " . (int)$order_info['order_id'] . " AND title = '" . $this->db->escape($data['text_total']) . "'");
						$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = " . (int)$order_info['order_id'] . ", code = 'total', title = '" . $this->db->escape($data['text_to_be_charged_separately']) . " (" . $subscription['product'] . ")', value = " . (float)-$subscription['cost'] . ", sort_order = " . ((int)$this->config->get('total_sort_order')-1));
					}
				}
			}
		}
		
		// Confirm order
		$strong = '<strong style="display: inline-block; width: 205px; padding: 2px 5px">';
		$comment = $strong . 'BRAINTREE TRANSACTION DATA</strong><br /><br />';
		
		$verification_codes = array(
			''  => '(Unchecked)',
			'M'	=> 'Matches',
			'N'	=> 'Does Not Match',
			'U'	=> 'Not Verified',
			'I'	=> 'Not Provided',
			'A'	=> 'Not Applicable',
		);
		
		if (!empty($transaction)) {
			$comment .= '<script type="text/javascript" src="view/javascript/braintree.js"></script>';
			$comment .= $strong . 'Charge Transaction ID:</strong>' . $transaction->id . '<br />';
			$comment .= $strong . 'Charge Transaction Status:</strong>' . $transaction->status . '<br />';
			$comment .= $strong . 'Charge Amount:</strong>' . $this->currency->format($transaction->amount, strtoupper($transaction->currencyIsoCode), 1) . '<br />';
			$comment .= $strong . 'Submitted for Settlement:</strong>' . ($transaction->status != 'authorized' ? 'Yes' : '<span>No &nbsp;</span> <a style="cursor: pointer" onclick="braintreeSubmit($(this), \'' . $transaction->id . '\')">(Submit Now)</a>') . '<br />';
			$comment .= $strong . 'Payment Method Type:</strong>' . $transaction->paymentInstrumentType . '<br />';
			if ($transaction->paymentInstrumentType == 'credit_card') {
				$comment .= $strong . 'Card Name:</strong>' . $transaction->creditCardDetails->cardholderName . '<br />';
				$comment .= $strong . 'Card Number:</strong>**** **** **** ' . $transaction->creditCardDetails->last4 . '<br />';
				$comment .= $strong . 'Card Expiry:</strong>' . $transaction->creditCardDetails->expirationMonth . ' / ' . $transaction->creditCardDetails->expirationYear . '<br />';
				$comment .= $strong . 'Card Type:</strong>' . $transaction->creditCardDetails->cardType . '<br />';
			} elseif ($transaction->paymentInstrumentType == 'paypal_account') {
				$comment .= $strong . 'PayPal E-mail:</strong>' . $transaction->paypalDetails->payerEmail . '<br />';
				$comment .= $strong . 'PayPal Payment ID:</strong>' . $transaction->paypalDetails->paymentId . '<br />';
				$comment .= $strong . 'PayPal Authorization ID:</strong>' . $transaction->paypalDetails->authorizationId . '<br />';
			}
			$comment .= $strong . 'CVV Response Code:</strong>' . $transaction->cvvResponseCode . ' = ' . $verification_codes[$transaction->cvvResponseCode] . '<br />';
			$comment .= $strong . 'AVS Street Response Code:</strong>' . $transaction->avsStreetAddressResponseCode . ' = ' . $verification_codes[$transaction->avsStreetAddressResponseCode] . '<br />';
			$comment .= $strong . 'AVS Postcode Response Code:</strong>' . $transaction->avsPostalCodeResponseCode . ' = ' . $verification_codes[$transaction->avsPostalCodeResponseCode] . '<br />';
			$comment .= $strong . 'Refund:</strong><a style="cursor: pointer" onclick="braintreeRefund($(this), \'' . $transaction->id . '\')">(Refund)</a>';
		}
		foreach ($plans_subscribed as $plan) {
			$comment .= '<hr />';
			$comment .= $strong . 'Subscription Plan:</strong>' . $plan->subscription->planId . '<br />';
			$comment .= $strong . 'Subscription Status:</strong>' . $plan->subscription->status . '<br />';
			if (!empty($plan->subscription->transactions)) {
				$transaction = $result->subscription->transactions[0];
				$comment .= $strong . 'Subscription Transaction ID:</strong>' . $transaction->id. '<br />';
				$comment .= $strong . 'Subscription Transaction Status:</strong>' . $transaction->status . '<br />';
				$comment .= $strong . 'Charge Amount:</strong>' . $this->currency->format($transaction->amount, strtoupper($transaction->currencyIsoCode), 1) . '<br />';
				$comment .= $strong . 'Payment Method Type:</strong>' . $transaction->paymentInstrumentType . '<br />';
				if ($transaction->paymentInstrumentType == 'credit_card') {
					$comment .= $strong . 'Card Name:</strong>' . $transaction->creditCardDetails->cardholderName . '<br />';
					$comment .= $strong . 'Card Number:</strong>**** **** **** ' . $transaction->creditCardDetails->last4 . '<br />';
					$comment .= $strong . 'Card Expiry:</strong>' . $transaction->creditCardDetails->expirationMonth . ' / ' . $transaction->creditCardDetails->expirationYear . '<br />';
					$comment .= $strong . 'Card Type:</strong>' . $transaction->creditCardDetails->cardType . '<br />';
				} elseif ($transaction->paymentInstrumentType == 'paypal_account') {
					$comment .= $strong . 'PayPal E-mail:</strong>' . $transaction->paypalDetails->payerEmail . '<br />';
					$comment .= $strong . 'PayPal Payment ID:</strong>' . $transaction->paypalDetails->paymentId . '<br />';
					$comment .= $strong . 'PayPal Authorization ID:</strong>' . $transaction->paypalDetails->authorizationId . '<br />';
				}
				$comment .= $strong . 'CVV Response Code:</strong>' . $transaction->cvvResponseCode . ' = ' . $verification_codes[$transaction->cvvResponseCode] . '<br />';
				$comment .= $strong . 'AVS Street Response Code:</strong>' . $transaction->avsStreetAddressResponseCode . ' = ' . $verification_codes[$transaction->avsStreetAddressResponseCode] . '<br />';
				$comment .= $strong . 'AVS Postcode Response Code:</strong>' . $transaction->avsPostalCodeResponseCode . ' = ' . $verification_codes[$transaction->avsPostalCodeResponseCode] . '<br />';
			}
		}
		
		$error_display = $this->config->get('config_error_display');
		$this->config->set('config_error_display', 0);
		if (version_compare(VERSION, '2.0') < 0) {
			$this->model_checkout_order->confirm($order_info['order_id'], $settings['order_status_id']);
			$this->model_checkout_order->update($order_info['order_id'], $settings['order_status_id'], $comment, false);
		} else {
			$this->model_checkout_order->addOrderHistory($order_info['order_id'], $settings['order_status_id'], $comment, false);
		}
		$this->config->set('config_error_display', $error_display);
		
		$this->response->setOutput(json_encode(array('success' => $this->url->link('checkout/success', '', 'SSL'))));
	}
	
	public function webhook() {
		$this->loadBraintree();
		$settings = $this->getSettings();
		
		$data = array();
		$data = array_merge($data, $this->load->language('total/total'));
		$data = array_merge($data, $this->load->language($this->type . '/' . $this->name));
		
		if (!empty($this->request->get['bt_challenge'])) {
			echo Braintree_WebhookNotification::verify($this->request->get['bt_challenge']);
			return;
		}
		
		if (!isset($this->request->get['key']) || $this->request->get['key'] != $this->config->get('config_encryption')) {
			$this->log->write(strtoupper($this->name) . ' WEBHOOK ERROR: webhook URL key ' . $this->request->get['key'] . ' does not match the encryption key ' . $this->config->get('config_encryption'));
			return;
		}
		
		if (empty($settings['subscriptions'])) {
			$this->log->write(strtoupper($this->name) . ' WEBHOOK ERROR: subscription products are not enabled');
			return;
		}
		
		$webhook = Braintree_WebhookNotification::parse($this->request->post['bt_signature'], $this->request->post['bt_payload']);
		
		if ($webhook->kind == 'subscription_charged_successfully') {
			$braintree_plan = $webhook->subscription->planId;
			$braintree_transaction = $webhook->subscription->transactions[0];
			$braintree_currency = $braintree_transaction->currencyIsoCode;
			$braintree_customer = $braintree_transaction->customer;
			$braintree_customer_id = explode('_', $braintree_customer['id']);
			$braintree_billing = $braintree_transaction->billing;
			$braintree_shipping = $braintree_transaction->shipping;
			
			$now_query = $this->db->query("SELECT NOW()");
			$last_order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE email = '" . $this->db->escape($braintree_customer['email']) . "' ORDER BY date_added DESC");
			if ($last_order_query->num_rows && (strtotime($now_query->row['NOW()']) - strtotime($last_order_query->row['date_added'])) < 600) {
				// Customer's last order is within 10 minutes, so it most likely was an immediate subscription and is already shown on their last order
				return;
			}
			
			if ($braintree_customer_id[0] == 'guest') {
				if ($settings['prevent_guests']) {
					$this->log->write(strtoupper($this->name) . ' WEBHOOK ERROR: customer with Braintree ID ' . $braintree_customer_id[0] . '_' . $braintree_customer_id[1] . ' does not exist in OpenCart, and guests are currently prevented from using subscriptions');
					return;
				}
				$data = array(
					'customer_id'		=> 0,
					'customer_group_id'	=> $this->config->get('config_customer_group_id'),
					'firstname'			=> $braintree_customer['firstName'],
					'lastname'			=> $braintree_customer['lastName'],
					'email'				=> $braintree_customer['email'],
					'telephone'			=> $braintree_customer['phone'],
					'fax'				=> $braintree_customer['fax']
				);
			} else {
				$customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = " . (int)$braintree_customer_id[1]);
				if (!$customer_query->num_rows) {
					$this->log->write(strtoupper($this->name) . ' WEBHOOK ERROR: customer with OpenCart ID ' . $braintree_customer_id[1] . ' does not exist in OpenCart, so subscription cannot be charged');
					return;
				}
				$data = array(
					'customer_id'		=> $customer_query->row['customer_id'],
					'customer_group_id'	=> $customer_query->row['customer_group_id'],
					'firstname'			=> $customer_query->row['firstname'],
					'lastname'			=> $customer_query->row['lastname'],
					'email'				=> $customer_query->row['email'],
					'telephone'			=> $customer_query->row['telephone'],
					'fax'				=> $customer_query->row['fax']
				);
			}
			
			$data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
			$data['store_id'] = $this->config->get('config_store_id');
			$data['store_name'] = $this->config->get('config_name');
			$data['store_url'] = ($data['store_id']) ? $this->config->get('config_url') : HTTP_SERVER;
			
			$billing_country = (!empty($braintree_billing['countryCodeAlpha2'])) ? $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE iso_code_2 = '" . $this->db->escape($braintree_billing['countryCodeAlpha2']) . "'") : array();
			$billing_zone = (!empty($braintree_billing['region'])) ? $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE `name` = '" . $this->db->escape($braintree_billing['region']) . "' AND country_id = '" . $this->db->escape($billing_country->row['country_id']) . "'") : array();
			
			$data['payment_firstname']		= $braintree_billing['firstName'];
			$data['payment_lastname']		= $braintree_billing['lastName'];
			$data['payment_company']		= $braintree_billing['company'];
			$data['payment_company_id']		= '';
			$data['payment_tax_id']			= '';
			$data['payment_address_1']		= $braintree_billing['streetAddress'];
			$data['payment_address_2']		= $braintree_billing['extendedAddress'];
			$data['payment_city']			= $braintree_billing['locality'];
			$data['payment_postcode']		= $braintree_billing['postalCode'];
			$data['payment_zone']			= $braintree_billing['region'];
			$data['payment_zone_id']		= (isset($billing_zone->row['zone_id'])) ? $billing_zone->row['zone_id'] : '';
			$data['payment_country']		= $braintree_billing['countryName'];
			$data['payment_country_id']		= (isset($billing_country->row['country_id'])) ? $billing_country->row['country_id'] : '';
			$data['payment_address_format']	= (isset($billing_country->row['address_format'])) ? $billing_country->row['address_format'] : '';
			
			$data['payment_method'] = $settings['title'][$this->config->get('config_language')];
			$data['payment_code'] = $this->name;
			
			$shipping_country = (!empty($braintree_shipping['countryCodeAlpha2'])) ? $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE iso_code_2 = '" . $this->db->escape($braintree_shipping['countryCodeAlpha2']) . "'") : array();
			$shipping_zone = (!empty($braintree_shipping['region'])) ? $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE `name` = '" . $this->db->escape($braintree_shipping['region']) . "' AND country_id = '" . $this->db->escape($shipping_country->row['country_id']) . "'") : array();
			
			$data['shipping_firstname']			= $braintree_shipping['firstName'];
			$data['shipping_lastname']			= $braintree_shipping['lastName'];
			$data['shipping_company']			= $braintree_shipping['company'];
			$data['shipping_company_id']		= '';
			$data['shipping_tax_id']			= '';
			$data['shipping_address_1']			= $braintree_shipping['streetAddress'];
			$data['shipping_address_2']			= $braintree_shipping['extendedAddress'];
			$data['shipping_city']				= $braintree_shipping['locality'];
			$data['shipping_postcode']			= $braintree_shipping['postalCode'];
			$data['shipping_zone']				= $braintree_shipping['region'];
			$data['shipping_zone_id']			= (isset($shipping_zone->row['zone_id'])) ? $shipping_zone->row['zone_id'] : '';
			$data['shipping_country']			= $braintree_shipping['countryName'];
			$data['shipping_country_id']		= (isset($shipping_country->row['country_id'])) ? $shipping_country->row['country_id'] : '';
			$data['shipping_address_format']	= (isset($shipping_country->row['address_format'])) ? $shipping_country->row['address_format'] : '';
			
			$product_data = array();
			$total_data = array();
			
			$subtotal = 0;
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "') WHERE p.location = '" . $this->db->escape($braintree_plan) . "'");
			if (!$product_query->num_rows) {
				$this->log->write(strtoupper($this->name) . ' WEBHOOK ERROR: the product associated with plan "' . $braintree_plan . '" does not exist in OpenCart');
				return;
			}
			
			$order_product_query = $this->db->query("SELECT DISTINCT product_id FROM " . DB_PREFIX . "order_product op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id) WHERE o.customer_id = " . (int)$data['customer_id']);
			$previous_order_products = array();
			foreach ($order_product_query->rows as $order_product) {
				$previous_order_products[] = $order_product['product_id'];
			}
			
			foreach ($product_query->rows as $product) {
				if (!in_array($product['product_id'], $previous_order_products)) continue;
				
				$subtotal += $braintree_transaction->amount;
				$product_data[] = array(
					'product_id' => $product['product_id'],
					'name'       => $product['name'],
					'model'      => $product['model'],
					'option'     => array(),
					'download'   => array(),
					'quantity'   => 1,
					'subtract'   => $product['subtract'],
					'price'      => $braintree_transaction->amount,
					'total'      => $braintree_transaction->amount,
					'tax'        => $this->tax->getTax($braintree_transaction->amount, $product['tax_class_id']),
					'reward'     => isset($product['reward']) ? $product['reward'] : 0
				);
			}
			
			$this->load->language('total/sub_total');
			$total_data[] = array(
				'code'			=> 'sub_total',
				'title'			=> $this->language->get('text_sub_total'),
				'text'			=> $this->currency->format($subtotal, $braintree_currency),
				'value'			=> $subtotal,
				'sort_order'	=> 1
			);
			
			/*
			foreach ($braintree_transaction->addOns as $addon) {
				$total_data[] = array(
					'code'			=> $addon->id,
					'title'			=> $addon->name,
					'text'			=> $this->currency->format($addon->amount, $braintree_currency),
					'value'			=> $addon->amount,
					'sort_order'	=> 2
				);
			}
			*/
			
			$this->load->language('total/total');
			$total_data[] = array(
				'code'			=> 'total',
				'title'			=> $this->language->get('text_total'),
				'text'			=> $this->currency->format($braintree_transaction->amount, $braintree_currency),
				'value'			=> $braintree_transaction->amount,
				'sort_order'	=> 3
			);
			
			$data['products'] = $product_data;
			$data['vouchers'] = array();
			$data['totals'] = $total_data;
			$data['total'] = $braintree_transaction->amount;
			
			$data['comment'] = '';
			$data['affiliate_id'] = 0;
			$data['commission'] = 0;
			
			$currency_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "currency WHERE code = '" . $braintree_currency . "'");
			$data['currency_id'] = $currency_query->row['currency_id'];
			$data['currency'] = $braintree_currency;
			$data['currency_code'] = $braintree_currency;
			$data['value'] = $this->currency->getValue($braintree_currency);
			$data['currency_value'] = $this->currency->getValue($braintree_currency);
			
			$data['language_id'] = $this->config->get('config_language_id');
			$data['ip'] = $this->request->server['REMOTE_ADDR'];
			$data['user_agent'] = (isset($this->request->server['HTTP_USER_AGENT'])) ? $this->request->server['HTTP_USER_AGENT'] : '';
			$data['accept_language'] = (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) ? $this->request->server['HTTP_ACCEPT_LANGUAGE'] : '';
			
			if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
				$data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];	
			} elseif(!empty($this->request->server['HTTP_CLIENT_IP'])) {
				$data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];	
			} else {
				$data['forwarded_ip'] = '';
			}	
			
			$this->load->model('checkout/order');
			$order_id = $this->model_checkout_order->addOrder($data);
			$order_status_id = $settings['order_status_id'];
			
			$strong = '<strong style="display: inline-block; width: 205px; padding: 2px 5px">';
			$comment = $strong . 'BRAINTREE TRANSACTION DATA</strong><br />';
			$comment .= $strong . 'Transaction Status:</strong>' . $braintree_transaction->status . '<br />';
			$comment .= $strong . 'Transaction ID:</strong>' . $braintree_transaction->id . '<br />';
			
			$error_display = $this->config->get('config_error_display');
			$this->config->set('config_error_display', 0);
			if (version_compare(VERSION, '2.0') < 0) {
				$this->model_checkout_order->confirm($order_id, $order_status_id);
				$this->model_checkout_order->update($order_id, $order_status_id, $comment, false);
			} else {
				$this->model_checkout_order->addOrderHistory($order_id, $order_status_id, $comment, false);
			}
			$this->config->set('config_error_display', $error_display);
		}
	}
}
?>