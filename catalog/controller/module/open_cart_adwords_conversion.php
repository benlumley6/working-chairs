<?php
class ControllerModuleOpenCartAdwordsConversion extends Controller {
	protected function index($setting) {

		$this->language->load('module/open_cart_adwords_conversion');
      	$this->data['heading_title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_name'));
		$this->data['description'] = html_entity_decode($setting['description'][$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
		if (isset($this->session->data['totalOrder'])) {
		$this->data['totalOrder'] = $this->session->data['totalOrder'];
	    } else { 
	    	$this->data['totalOrder'] = 0;
	    }
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/open_cart_adwords_conversion.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/open_cart_adwords_conversion.tpl';
		} else {
			$this->template = 'default/template/module/open_cart_adwords_conversion.tpl';
		}
		
		$this->render();
	}
}
?>