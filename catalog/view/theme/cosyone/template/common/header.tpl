<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<script type="text/javascript">var noveroAccount = 'C334150'; var noveroCampaign = '523'; var noveroNumber = '01623 242 550'; var noveroNumberPlain = '01623242550';(function() {var script = document.createElement('script'); script.type = 'text/javascript';script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'www.noveroplatform.com/js/tracker.js';script.id = 'noveroJs';document.getElementsByTagName('head')[0].appendChild(script);})();</script>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/cosyone/stylesheet/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/cosyone/stylesheet/grid/<?php echo $this->config->get('cosyone_max_width')?>.css" />
<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<?php if($this->config->get('cosyone_use_responsive') == 'enabled'){ ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/cosyone/stylesheet/responsive.css" />
<?php } ?>
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="catalog/view/theme/cosyone/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="catalog/view/theme/cosyone/js/cosyone_common.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/tabs.js"></script>

<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<!--[if IE 8]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/cosyone/stylesheet/ie8.css" />
<![endif]-->
<?php if ($stores) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
//--></script>
<?php } ?>
<?php echo $google_analytics; ?>
<!-- Custom css -->
<?php if($this->config->get('cosyone_use_custom_css') == 'enabled'){ ?>
<style>
<?php echo html_entity_decode($this->config->get('cosyone_custom_css_content'), ENT_QUOTES, 'UTF-8'); ?>
</style>
<?php } ?>
<!-- Custom styling -->
<?php if($this->config->get('cosyone_use_custom') == 'enabled'){ require( 'catalog/view/theme/cosyone/template/common/custom_style.tpl' ); } ?>
<!-- Custom script -->
<?php if($this->config->get('cosyone_use_custom_javascript') == 'enabled'){ ?>
<script type="text/javascript">
$(document).ready(function() {
<?php echo html_entity_decode($this->config->get('cosyone_custom_javascript_content'), ENT_QUOTES, 'UTF-8'); ?>
});
</script>
<?php } ?>
<!-- Custom fonts -->
<?php if($this->config->get('cosyone_use_custom_font') == 'enabled'){ ?>
<?php require( 'catalog/view/theme/cosyone/template/common/custom_fonts.tpl' ); ?>
<?php } else { ?>
<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,500,600,700' rel='stylesheet' type='text/css'>  
<?php } ?>
<!-- BEGIN: Google Certified Shops -->
<script type="text/javascript">
  var gts = gts || [];

  gts.push(["id", "638342"]);
  gts.push(["badge_position", "BOTTOM_RIGHT"]);
  gts.push(["locale", "PAGE_LANGUAGE"]);
  gts.push(["google_base_offer_id", "ITEM_GOOGLE_SHOPPING_ID"]);
  gts.push(["google_base_subaccount_id", "ITEM_GOOGLE_SHOPPING_ACCOUNT_ID"]);
  gts.push(["google_base_country", "ITEM_GOOGLE_SHOPPING_COUNTRY"]);
  gts.push(["google_base_language", "ITEM_GOOGLE_SHOPPING_LANGUAGE"]);

  (function() {
    var gts = document.createElement("script");
    gts.type = "text/javascript";
    gts.async = true;
    gts.src = "https://www.googlecommerce.com/trustedstores/api/js";
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(gts, s);
  })();
</script>
<!-- END: Google Certified Shops -->
</head>
<body>
<!-- Old Internet explorer check -->
<?php if(isset($_SERVER['HTTP_USER_AGENT'])){ ?>
<?php if((preg_match('/(?i)msie [2-8]/',$_SERVER['HTTP_USER_AGENT'])) && ($this->config->get('cosyone_use_ie'))){ require( 'catalog/view/theme/cosyone/template/common/old_browser.tpl' ); } ?>
<?php } ?>
<!-- Cookie message -->
<?php if((!isset($_COOKIE['cookie_check'])) && ($this->config->get('cosyone_use_cookie'))){ require( 'catalog/view/theme/cosyone/template/common/cookie_control.tpl' ); } ?>
<div class="outer_container 
<?php echo $this->config->get('cosyone_default_product_style')?> 
<?php if($this->config->get('cosyone_use_custom') == 'enabled'){ echo $this->config->get('cosyone_container_layout'); } ?> 
<?php echo $this->config->get('cosyone_use_breadcrumb')?>">
<div class="header_wrapper 
<?php echo $this->config->get('cosyone_menu_sticky')?> 
<?php echo $this->config->get('cosyone_menu_border')?> 
<?php echo $this->config->get('cosyone_header_style')?>">
<div class="header_top_line_wrapper">
<div class="header_top_line inner_container">
<?php if($this->config->get('cosyone_header_style') == 'header1'){ ?>
    <?php } else { ?>
    <div class="drop_downs_wrapper">
    <?php echo $header_login; ?>
	<?php echo $language; ?>
  	<?php echo $currency; ?>
    </div>
    <?php } ?>
  <div class="promo_message">
  <?php echo html_entity_decode($this->config->get('cosyone_top_promo_message_' . $this->config->get('config_language_id')), ENT_QUOTES, 'UTF-8'); ?>
  </div>
  <div class="links contrast_font">
  <a href="<?php echo $account; ?>"><?php echo $text_account; ?></a>
  <a href="<?php echo $wishlist; ?>" id="wishlist-total"><?php echo $text_wishlist; ?></a>
  <a href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a>
  <a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a>
  </div>
  <div class="clearfix"></div>
</div>
</div>
<div class="inner_container header">
<div class="header_main">
  <div class="header_right"> 
  <?php if ($logo) { ?>
  <div class="logo"><a href="http://www.workingchairs.co.uk/"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a></div>
  <?php } ?>
 <?php if($this->config->get('cosyone_header_style') == 'header3') { ?>
 <?php } else { ?>
 <?php if($this->config->get('cosyone_header_search') == 'enabled') { ?>
 <div id="search">
 <i class="fa fa-search button-search"></i>
 <input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
 </div>
 <?php } ?>
 <?php } ?>
 <div class="mobile_clear"></div>
  <!-- Position for header login, lang, curr, in the header main -->
	<?php if($this->config->get('cosyone_header_style') == 'header1'){ ?>
    <?php echo $header_login; ?>
	<?php echo $language; ?>
  	<?php echo $currency; ?>
    <?php } ?>
  <?php if($this->config->get('cosyone_header_style') == 'header2') { ?>
    	<div class="shortcuts_wrapper">
       	<?php echo $header_wishlist_compare; ?>
        <?php echo $cart; ?>
        </div>
    	<?php } else { ?>
    <?php } ?>
  <?php if($this->config->get('cosyone_header_style') == 'header3'){ ?>
    <?php } else { ?>
      </div> <!-- header_right ends -->
	</div> <!-- header ends -->
    <?php } ?>
<?php if ($categories) { ?>
<div class="menu_wrapper">
<div class="inner_container menu_border"></div>
<div class="inner_container menu_holder">
<div id="menu">
<?php if($this->config->get('cosyone_header_style') == 'header2') { ?>
    	<?php } else { ?>
        <div class="shortcuts_wrapper">
        <?php if(($this->config->get('cosyone_header_style') == 'header3') && ($this->config->get('cosyone_header_search') == 'enabled')) { ?>
 <div class="search-holder">
 <div id="search">
 <i class="fa fa-search button-search"></i>
 <input type="text" name="search" class="search_input" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
 </div></div>
 <?php } ?>
        <?php echo $header_wishlist_compare; ?>
        <?php echo $cart; ?>
        </div>
 <?php } ?>
<a class="mobile_menu_trigger up_to_tablet"><i class="fa fa-bars"></i> <?php echo $cosyone_text_mobile_menu; ?></a>
  <ul class="only_desktop">
	<li class="home only_desktop <?php echo $this->config->get('cosyone_show_home_icon') ?>"><a href="http://www.workingchairs.co.uk/"><?php echo $text_home; ?></a></li>
       <?php foreach ($categories as $category_1) { ?>
        <?php if ($category_1['category_1_id'] == $category_1_id) { ?>
		<li class="col<?php echo $category_1['column']; ?> current"><a href="<?php echo $category_1['href']; ?>" ><?php echo $category_1['name']; ?><i class="fa fa-sort-desc"></i></a>
         <?php } else { ?>
         <li class="col<?php echo $category_1['column']; ?>"><a href="<?php echo $category_1['href']; ?>" ><?php echo $category_1['name']; ?><i class="fa fa-sort-desc"></i></a>
         <?php } ?>
          <?php if ($category_1['children']) { ?>
          <div class="menu_drop_down" style="width: <?php echo ((($category_1['column']) * (195)) + (10)); ?>px">
          <div class="wrapper">
          <ul><?php foreach ($category_1['children'] as $category_2) { ?>
          <li class="column level2">
            <a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?><i class="fa fa-caret-right"></i></a>
            <?php if($this->config->get('cosyone_menu_mega_second_thumb') == 'enabled' && $category_2['thumb']) { ?>
          <img src="<?php echo $category_2['thumb']; ?>" alt="<?php echo $category_2['name']; ?>"/>
          <?php } ?>
              <?php if ($category_2['children']) { ?>
              <div class="third">
              <ul>
               <?php foreach ($category_2['children'] as $category_3) { ?>
               <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
               <?php } ?>
              </ul>
              </div>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          </div><!-- wrapper ends -->
          </div>
          <?php } ?>
        </li>
        <?php } ?>
        <?php if($this->config->get('cosyone_custom_menu_block') == 'enabled'){ ?>
		<li class="withsubs custom_block"><a><?php echo ($this->config->get('cosyone_custom_menu_block_title_' . $this->config->get('config_language_id'))) ?><i class="fa fa-sort-desc"></i></a>
        <div class="menu_drop_down" style="width:<?php echo $this->config->get('cosyone_menu_block_width') ?>px"><?php echo html_entity_decode($this->config->get('cosyone_menu_custom_block_' . $this->config->get('config_language_id')), ENT_QUOTES, 'UTF-8'); ?></div></li>
		<?php } ?>
        <?php if($this->config->get('cosyone_custom_menu_link1') == 'enabled'){ ?>
		<li><a href="<?php echo $this->config->get('cosyone_custom_menu_url1')?>"><?php echo ($this->config->get('cosyone_custom_menu_title1_' . $this->config->get('config_language_id'))) ?></a></li>
        <?php } ?>
        <?php if($this->config->get('cosyone_custom_menu_link2') == 'enabled'){ ?>
		<li><a href="<?php echo $this->config->get('cosyone_custom_menu_url2')?>"><?php echo ($this->config->get('cosyone_custom_menu_title2_' . $this->config->get('config_language_id'))) ?></a></li>
		<?php } ?>
      </ul>
    </div> <!-- menu_holder ends -->
</div> <!-- menu ends -->
</div> <!-- menu_wrapper ends -->
<?php } ?>
<?php if($this->config->get('cosyone_header_style') == 'header3'){ ?>
      </div> <!-- header_right ends -->
	</div> <!-- header ends -->
    <?php } else { ?><?php } ?>
<div class="clearfix"></div>
<div class="mobile_menu_wrapper">

<div class="mobile_menu">
<?php if ($categories) { ?>
	<ul>
       <?php foreach ($categories as $category_1) { ?>
         <li><a href="<?php echo $category_1['href']; ?>" ><?php echo $category_1['name']; ?></a>
          <?php if ($category_1['children']) { ?>
          <span class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
          <ul>
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <li>
            <a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
              <?php if ($category_2['children']) { ?>
              <span class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>
              <ul>
                <?php foreach ($category_2['children'] as $category_3) { ?>
                <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
        <?php if($this->config->get('cosyone_custom_menu_link1') == 'enabled'){ ?>
		<li><a href="<?php echo $this->config->get('cosyone_custom_menu_url1')?>"><?php echo ($this->config->get('cosyone_custom_menu_title1_' . $this->config->get('config_language_id'))) ?></a></li>
		<?php } ?>
        <?php if($this->config->get('cosyone_custom_menu_link2') == 'enabled'){ ?>
		<li><a href="<?php echo $this->config->get('cosyone_custom_menu_url2')?>"><?php echo ($this->config->get('cosyone_custom_menu_title2_' . $this->config->get('config_language_id'))) ?></a></li>
		<?php } ?>
      </ul>
<?php } ?>
</div>
</div>
<?php if ($error) { ?>
<div class="warning"><?php echo $error ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<div id="notification"></div>
</div> <!-- header_wrapper ends -->
</div> <!-- inner conainer ends -->
<div class="breadcrumb_wrapper"></div>
<div class="inner_container main">