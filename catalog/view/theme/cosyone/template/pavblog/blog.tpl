<?php echo $header; ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  
  
  <div class="pav-blog">
		<?php if( $blog['thumb_large'] ) { ?>
			<div class="image zoom_image_container">
				<img src="<?php echo $blog['thumb_large'];?>" title="<?php echo $blog['title'];?>" class="zoom_image"/>
			</div>
			<?php } ?>
		
        <div class="summary">
        
        <div class="left">
	<?php if( $config->get('blog_show_created') ) { ?>
		<div class="left contrast_font">
            <span class="date_added secondary_background">
			<span class="day"><?php echo date("d",strtotime($blog['created']));?></span>
			<span class="month"><?php echo date("M",strtotime($blog['created']));?></span>
			</span>
     <?php } ?>       
            <?php if( $config->get('blog_show_comment_counter') ) { ?>
        	<span class="comment_count"><i class="fa fa-comments"></i> <?php echo $blog['comment_count'];?></span>
			<?php } ?>
            
			<div class="social-heading main_font"><?php echo $this->language->get('text_like_this');?></div>
            <!-- AddThis Button BEGIN -->
			<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
			<a class="addthis_button_preferred_1"></a>
			<a class="addthis_button_preferred_2"></a>
			<a class="addthis_button_preferred_3"></a>
			<a class="addthis_button_preferred_4"></a>
			<a class="addthis_button_compact"></a>
			</div>
            <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js"></script>
			<!-- AddThis Button END -->

        </div>
	
    </div> <!-- left ends -->
        
        <div class="right">
        <h1 class="blog-title"><?php echo $heading_title; ?></h1>
        
        <div class="blog-meta">
			
			<?php if( $config->get('blog_show_author') ) { ?>
			<span class="author"><span><?php echo $this->language->get("text_write_by");?></span> <?php echo $blog['author'];?></span>
			<?php } ?>
			<?php if( $config->get('blog_show_category') ) { ?>
			<span class="publishin">
				<span><?php echo $this->language->get("text_published_in");?></span>
				<a href="<?php echo $blog['category_link'];?>" title="<?php echo $blog['category_title'];?>"><?php echo $blog['category_title'];?></a>
			</span>
			<?php } ?>
			
			<?php if( $config->get('blog_show_hits') ) { ?>
			<span class="hits"><span><?php echo $this->language->get("text_hits");?></span> <?php echo $blog['hits'];?></span>
			<?php } ?>
			
		</div>
        
        <div class="description"><?php echo $description;?></div>
        
         <div class="blog-content">
			<div class="content-wrap">
			<?php echo $content;?>
			</div>
		<?php if( $blog['video_code'] ) { ?>
		<div class="pav-video clearfix"><?php echo html_entity_decode($blog['video_code'], ENT_QUOTES, 'UTF-8');?></div>
		<?php } ?>
		 </div>
         
         <?php if( !empty($tags) ) { ?>
		 <div class="blog-tags">
			<span class="contrast_font"><?php echo $this->language->get('text_tags');?></span>
			<?php foreach( $tags as $tag => $tagLink ) { ?>
				<a href="<?php echo $tagLink; ?>" title="<?php echo $tag; ?>"><?php echo $tag; ?></a>
			<?php } ?>
		 </div>
		 <?php } ?>
         
        </div> <!-- right ends -->
        
        </div> <!-- summary ends -->	
            
		
		
		 

		 
		 <div class="items_wrapper pavcol2">
				<?php if( !empty($samecategory) ) { ?>
				<div class="blog-item">
					<div class="box-heading blog_related"><?php echo $this->language->get('text_in_same_category');?></div>
					<ul class="related">
						<?php foreach( $samecategory as $item ) { ?>
						<li><a href="<?php echo $this->url->link('pavblog/blog',"id=".$item['blog_id']);?>"><?php echo $item['title'];?></a></li>
						<?php } ?>
					</ul>
				</div>
				<?php } ?>
				<?php if( !empty($related) ) { ?>
				<div class="blog-item">
					<div class="box-heading blog_related"><?php echo $this->language->get('text_in_related_by_tag');?></div>
					<ul class="related">
						<?php foreach( $related as $item ) { ?>
						<li><a href="<?php echo $this->url->link('pavblog/blog',"id=".$item['blog_id']);?>"><?php echo $item['title'];?></a></li>
						<?php } ?>
					</ul>
				</div>
				<?php } ?>
		</div>
		
		 <div class="pav-comment">
			<?php if( $config->get('blog_show_comment_form') ) { ?>
				<?php if( $config->get('comment_engine') == 'diquis' ) { ?>
			    <div id="disqus_thread"></div>
					<script type="text/javascript">
						/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
						var disqus_shortname = '<?php echo $config->get('diquis_account');?>'; // required: replace example with your forum shortname

						/* * * DON'T EDIT BELOW THIS LINE * * */
						(function() {
							var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
							dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
							(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
						})();
					</script>
					<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
					<a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
    
				<?php } elseif( $config->get('comment_engine') == 'facebook' ) { ?>
				<div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) {return;}
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $config->get("facebook_appid");?>";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-comments" data-href="<?php echo $link; ?>" 
							data-num-posts="<?php echo $config->get("comment_limit");?>" data-width="<?php echo $config->get("facebook_width")?>">
					</div>
				<?php }else { ?>
					
                    <?php if( count($comments) ) { ?>
					<h4 class="comments heading"><?php echo $this->language->get('text_list_comments'); ?></h4>
					<div class="pave-listcomments">
						<?php foreach( $comments as $comment ) {  $default='';?>
						<div class="comment-item clearfix" id="comment<?php echo $comment['comment_id'];?>">
							
							<img src="<?php echo "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $comment['email'] ) ) ) . "?d=" . urlencode( $default ) . "&s=60" ?>" align="left"/>
							<div class="comment-wrap">
								<div class="comment-meta">
								<span class="comment-postedby contrast_font"><?php echo $comment['user'];?>,</span>
                                <span class="comment-created"><?php echo $comment['created'];?></span>
                                <a class="comment-link" href="<?php echo $link;?>#comment<?php echo $comment['comment_id'];?>"><i class="fa  fa-mail-reply"></i></a>
								</div>
								<?php echo $comment['comment'];?>
							</div>
						</div>
						<?php } ?>
                        </div>
						<div class="pagination">
							<?php echo $pagination;?>
						
					</div>
					<?php } ?>
					<h4 class="comments heading"><?php echo $this->language->get("text_leave_a_comment");?></h4>
                    
                    
					<form action="<?php echo $comment_action;?>" method="post" id="comment-form">
						<div class="message" style="display:none"></div>
                        <div class="content form comments">
						<div class="input_field_half">
							<b><?php echo $this->language->get('entry_name');?></b>
							<input type="text" name="comment[user]" value="" id="comment-user"/>
						</div>
						<div class="input_field_half right">
							<b><?php echo $this->language->get('entry_email');?></b>
							<input type="text" name="comment[email]" value="" id="comment-email"/>
						</div>
						<div class="input_field_full">
							<b><?php echo $this->language->get('entry_comment');?></b>
							<textarea name="comment[comment]"  id="comment-comment"></textarea>
						</div>
                        
						<?php if( $config->get('enable_recaptcha') ) { ?>
						<div class="input_field_half">
                        <div class="recaptcha">
							 <b><?php echo $entry_captcha; ?></b>
						    <img src="index.php?route=pavblog/blog/captcha" alt="" aligh="left"/>
						    <input type="text" name="captcha" value="<?php echo $captcha; ?>" size="10" />
                        </div>
                        </div>
						<?php } ?>
						<input type="hidden" name="comment[blog_id]" value="<?php echo $blog['blog_id']; ?>" />
                        <div class="clearfix"></div>
						<div class="buttons-wrap">
							<button class="btn btn-submit button" type="submit">
								<span><?php echo $this->language->get('text_submit');?></span>
							</button>
                            </div>
						</div>
					</form>
					<script type="text/javascript">
						$( "#comment-form .message" ).hide();
						$("#comment-form").submit( function(){
							$.ajax( {type: "POST",url:$("#comment-form").attr("action"),data:$("#comment-form").serialize(), dataType: "json",}).done( function( data ){
								if( data.hasError ){
									$( "#comment-form .message" ).html( data.message ).show();	
								}else {
									location.href='<?php echo str_replace("&amp;","&",$link);?>';
								}
							} );
							return false;
						} );
						
					</script>
				<?php } ?>
			<?php } ?>
		 </div>
  </div>

  
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>
