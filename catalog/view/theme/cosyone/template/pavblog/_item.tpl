<div class="blog-item">

	<?php if( $blog['thumb'] && $config->get('cat_show_image') )  { ?>
	<div class="image zoom_image_container">
    <a href="<?php echo $blog['link'];?>" title="<?php echo $blog['title'];?>"><img class="zoom_image" src="<?php echo $blog['thumb'];?>" title="<?php echo $blog['title'];?>"/></a>
    </div>
	<?php } ?>
	
    <div class="summary">
    
	
	<div class="left">
	<?php if( $config->get('cat_show_created') ) { ?>
		<div class="left contrast_font">
            <span class="date_added secondary_background">
			<span class="day"><?php echo date("d",strtotime($blog['created']));?></span>
			<span class="month"><?php echo date("M",strtotime($blog['created']));?></span>
			</span>
            
            <?php if( $config->get('cat_show_comment_counter') ) { ?>
        	<a href="<?php echo $blog['link'];?>" title="<?php echo $blog['title'];?>"><span class="comment_count"><i class="fa fa-comments"></i> <?php echo $blog['comment_count'];?></span></a>
			<?php } ?>
            
        </div>
	<?php } ?>
    </div> <!-- left ends -->
    
    <div class="right">
    
    <?php if( $config->get('cat_show_title') ) { ?>
	<h4 class="blog-title">	<a href="<?php echo $blog['link'];?>" title="<?php echo $blog['title'];?>"><?php echo $blog['title'];?></a></h4>
	<?php } ?>
    
    <div class="blog-meta">
		<?php if( $config->get('cat_show_author') ) { ?>
		<span class="author"><span><?php echo $this->language->get("text_write_by");?></span> <?php echo $blog['author'];?></span>
		<?php } ?>
		<?php if( $config->get('cat_show_category') ) { ?>
		<span class="publishin">
			<span><?php echo $this->language->get("text_published_in");?></span>
			<a href="<?php echo $blog['category_link'];?>" title="<?php echo $blog['category_title'];?>"><?php echo $blog['category_title'];?></a>
		</span>
		<?php } ?>
		
		<?php if( $config->get('cat_show_hits') ) { ?>
		<span class="hits"><span><?php echo $this->language->get("text_hits");?></span> <?php echo $blog['hits'];?></span>
		<?php } ?>
		
	</div> <!-- blog-meta ends -->
    
    
    
    <?php if( $config->get('cat_show_description') ) {?>
	<div class="description">
	<?php echo $blog['description'];?>
	</div>
	<?php } ?>
    
    <?php if( $config->get('cat_show_readmore') ) { ?>
	<a href="<?php echo $blog['link'];?>" class="button remove"><?php echo $this->language->get('text_readmore');?></a>
    <?php } ?>
    
    </div> <!-- right ends -->



</div> <!-- summary ends -->
</div> <!-- blog-item ends -->