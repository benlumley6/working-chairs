<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
	<div class="pav-header">
		<h1><?php echo $heading_title; ?></h1>
		<a class="rss-wrapper" href="<?php echo $category_rss;?>"><span class="icon-rss">Rss</span></a>	
	</div>  
  <div class="pav-category">
		<?php if( !empty($children) ) { ?>
			<div class="box-heading"><?php echo $this->language->get('text_children');?></div>
			<div class="items_wrapper">
				<?php 
				$cols = (int)$config->get('children_columns');
				foreach( $children as $key => $sub )  { $key = $key + 1;?>
						<div class="blog-item c<?php echo $cols;?>">
							<?php if( $sub['thumb'] ) { ?>
							<div class="image zoom_image_container"><a href="<?php echo $sub['link']; ?>" title="<?php echo $sub['title']; ?>"><img class="zoom_image" src="<?php echo $sub['thumb'];?>"/></a></div>
							<?php } ?>
							<a class="category_title contrast_font" href="<?php echo $sub['link']; ?>" title="<?php echo $sub['title']; ?>"><?php echo $sub['title']; ?> (<?php echo $sub['count_blogs']; ?>)</a>
							<div class="sub-description">
							<?php echo $sub['description']; ?>
							</div>
						</div>
					<?php if( ( $key%$cols==0 || $cols == count($leading_blogs)) ){ ?>
					<?php } ?>
				<?php } ?>
			</div>
		<?php } ?>
		<div class="pav-blogs">
			<?php
			$cols = $config->get('cat_columns_leading_blog');
			if( count($leading_blogs) ) { ?>
				<div class="items_wrapper pavcol<?php echo $cols;?> category">
					<?php foreach( $leading_blogs as $key => $blog ) { $key = $key + 1;?>
					<?php require( '_item.tpl' ); ?>
					<?php if( ( $key%$cols==0 || $cols == count($leading_blogs)) ){ ?>
					<?php } ?>
					<?php } ?>
				</div>
			<?php } ?>
			<?php
				$cols = $config->get('cat_columns_secondary_blogs');
				if ( count($secondary_blogs) ) { ?>
				<div class="items_wrapper pavcol<?php echo $cols;?> category">
					<?php foreach( $secondary_blogs as $key => $blog ) {  $key = $key+1; ?>
					<?php require( '_item.tpl' ); ?>
					<?php if( ( $key%$cols==0 || $cols == count($leading_blogs)) ){ ?>
					<?php } ?>
					<?php } ?>
				</div>
			<?php } ?>
			<?php if( $total ) { ?>	
			<div class="pav-pagination pagination"><?php echo $pagination;?></div>
			<?php } ?>
		</div>
  </div>
 <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>