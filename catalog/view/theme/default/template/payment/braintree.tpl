<?php
//==============================================================================
// Braintree Payment Gateway v210.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================
?>

<script type="text/javascript" src="https://js.braintreegateway.com/v2/braintree.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<?php if ($settings['input_styling'] == 'dropin') { ?>
	
	<form id="payment-form" method="post" action="">
		<div id="dropin"></div>
		<input type="submit" id="submit" style="display: none" />
	</form>
	<script type="text/javascript">
		function confirmOrder() {
			$('#submit').click();
		}
		
		if (!$('#dropin').html().length) {
			$('#dropin').html('<span style="font-size: 11px"><?php echo $text_loading; ?></span>');
			setTimeout(function(){
				$('#dropin').empty();
				braintree.setup('<?php echo $client_token; ?>', 'dropin', {
					container: 'dropin',
					paymentMethodNonceReceived: function (event, nonce) {
						chargeCard(nonce, 'dropin');
					}
				});
			}, 2000);
		}
	</script>
	
<?php } else { ?>
	
	<style type="text/css">
		#card-images, #card-select, #card-name, #card-number, #card-month, #card-year { display: inline-block; }
		#card-select { margin: 0 10px; width: 200px; }
		#new-card { padding: 10px; <?php if ($stored_cards) echo 'display: none;' ?> }
		#card-name { width: 200px; }
		#card-number { width: 200px; }
		#card-month { width: 47px; }
		#card-year { width: 47px; }
		#card-security { width: 60px; }
		#store-card { margin-top: 10px; }
	</style>
	<?php if (version_compare(VERSION, '2.0') < 0) { ?>
		<style type="text/css">
			fieldset { margin-bottom: 25px; }
			legend { font-size: 18px; }
			@media only screen and (min-width : 600px) { 
				.col-sm-2 { display: inline-block; width: 170px; height: 30px; }
				.col-sm-10 { display: inline-block; height: 30px; vertical-align: middle !important; }
			}
			@media only screen and (max-width : 600px) { 
				.col-sm-10 { margin-bottom: 5px; }
			}
		</style>
	<?php } ?>
	
	<form id="payment-form" class="form-horizontal">
		<?php if ($settings['input_styling'] == 'paypal') { ?>
			<fieldset>
				<legend><?php echo $text_pay_with_paypal; ?></legend>
				<div id="paypal-button"></div>
			</fieldset>
		<?php } ?>
		<br />
		<fieldset>
			<legend><?php echo $text_pay_with_card; ?></legend>
			
			<?php if ($stored_cards) { ?>
				<div class="form-group">
					<div class="col-sm-10" id="stored-cards" class="form-control">
						<div id="card-images">
							<img style="display: none" id="visa-image" src="https://assets.braintreegateway.com/payment_method_logo/visa.png" alt="Visa" />
							<img style="display: none" id="mastercard-image" src="https://assets.braintreegateway.com/payment_method_logo/mastercard.png" alt="MasterCard" />
							<img style="display: none" id="american_express-image" src="https://assets.braintreegateway.com/payment_method_logo/american_express.png" alt="American Express" />
							<img style="display: none" id="discover-image" src="https://assets.braintreegateway.com/payment_method_logo/discover.png" alt="Discover" />
							<img style="display: none" id="jcb-image" src="https://assets.braintreegateway.com/payment_method_logo/jcb.png" alt="JCB" />
						</div>
						<select id="card-select" name="token" class="form-control">
							<?php foreach ($stored_cards as $card) { ?>
								<option class="<?php echo $card['token']; ?>" value="<?php echo $card['token']; ?>" title="<?php echo $card['type']; ?>" <?php if ($card['is_default']) echo 'selected="selected"'; ?>><?php echo $card['name']; ?></option>
							<?php } ?>
							<option value="" title=""><?php echo $text_use_a_new_card; ?></option>
						</select>
						<a id="delete-card" class="<?php echo $settings['button_class']; ?>" onclick="if ($('#card-select').val()) deleteCard($('#card-select').val())"><?php echo $text_delete_this_card; ?></a>
					</div>
				</div>
			<?php } ?>
			
			<div id="new-card">
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_card_name; ?></label>
					<div class="col-sm-10">
						<input type="text" id="card-name" data-braintree-name="cardholder_name" class="form-control" value="<?php echo $order_info['firstname'] . ' ' . $order_info['lastname']; ?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_card_number; ?></label>
					<div class="col-sm-10">
						<input type="text" id="card-number" data-braintree-name="number" class="form-control" autocomplete="off" value="" /></td>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_card_type; ?></label>
					<div class="col-sm-10" id="card-type">
						<img width="36" height="24" src="https://assets.braintreegateway.com/payment_method_logo/visa.png" alt="Visa" />
						<img width="36" height="24" src="https://assets.braintreegateway.com/payment_method_logo/mastercard.png" alt="MasterCard" />
						<img width="36" height="24" src="https://assets.braintreegateway.com/payment_method_logo/american_express.png" alt="American Express" />
						<img width="36" height="24" src="https://assets.braintreegateway.com/payment_method_logo/discover.png" alt="Discover" />
						<img width="36" height="24" src="https://assets.braintreegateway.com/payment_method_logo/jcb.png" alt="JCB" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_card_expiry; ?></label>
					<div class="col-sm-10">
						<input type="text" id="card-month" data-braintree-name="expiration_month" class="form-control" maxlength="2" autocomplete="off" value="" placeholder="MM" />
						/ <input type="text" id="card-year" data-braintree-name="expiration_year" class="form-control" maxlength="2" autocomplete="off" value="" placeholder="YY" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_card_security; ?></label>
					<div class="col-sm-10">
						<input type="text" id="card-security" data-braintree-name="cvv" class="form-control" maxlength="4" autocomplete="off" value="" />
					</div>
				</div>
				<?php if ($logged_in && $settings['allow_stored_cards'] && $settings['store_card'] != 'never') { ?>					
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $text_store_card; ?></label>
						<div class="col-sm-10" style="vertical-align: top">
							<input type="checkbox" id="store-card" />
						</div>
					</div>
				<?php } ?>
			</div>
			
		</fieldset>
	</form>
	<script type="text/javascript">
		<?php if ($settings['input_styling'] == 'paypal') { ?>
			var loading = false;
			function loadPaypal() {
				$('#paypal-button').before('<span id="paypal-loading" style="font-size: 11px"><?php echo $text_loading; ?></span>');
				braintree.setup('<?php echo $client_token; ?>', 'paypal', {
					container: 'paypal-button',
				});
				setTimeout(function(){
					if ($('#paypal-button').html().length) {
						$('#paypal-loading').remove();
					} else {
						loadPaypal();
					}
				}, 1000);
			}
			loadPaypal();
		<?php } ?>
		
		function confirmOrder() {
			if ($('input[name="payment_method_nonce"]').val()) {
				chargeCard($('input[name="payment_method_nonce"]').val(), '');
			} else if ($('#new-card').is(':visible')) {
				var client = new braintree.api.Client({clientToken: '<?php echo $client_token; ?>'});
				client.tokenizeCard({
					number: $('#card-number').val(),
					cardholderName: $('#card-name').val(),
					expirationDate: $('#card-month').val() + '/' + $('#card-year').val(),
					cvv: $('#card-security').val(),
					billingAddress: {
						postalCode: '<?php echo $order_info['payment_postcode']; ?>',
					}
				}, function (err, nonce) {
					if (err) {
						displayError(err);
					} else {
						chargeCard(nonce, '');
					}
				});
			} else {
				chargeCard('', $('#card-select').val());
			}
		}
	</script>
	
<?php } ?>

<div class="buttons">
	<div class="right pull-right">
		<a id="button-confirm" onclick="confirmOrder()" class="<?php echo $settings['button_class']; ?>" style="<?php echo $settings['button_styling']; ?>">
			<?php echo $settings['button_text_' . $language]; ?>
		</a>
	</div>
</div>

<script type="text/javascript">
	$('#card-select').change(function(){
		$('#card-images img').hide();
		$('#card-images img[alt="' + $('#card-select option:selected').attr('title') + '"]').show();
		if (!$('#card-select').val()) {
			$('#delete-card').hide(); 
			$('#new-card').slideDown();
		} else {
			$('#delete-card').show();
			$('#new-card').slideUp();
		}
	});
	$('#card-select').change();
	
	$('#card-number').keyup(function(){
		$('#card-type img').css('opacity', 0.3);
		$('#card-type img[alt="' + Stripe.card.cardType($('#card-number').val()) + '"]').css('opacity', 1);
	});
	
	function displayWait() {
		$('#button-confirm').removeAttr('onclick').attr('disabled', 'disabled');
		$('#card-select').attr('disabled', 'disabled');
		$('.alert').remove();
		$('#payment-form').after('<div class="attention alert alert-warning" style="display: none"><?php echo $text_please_wait; ?></div>');
		$('.attention').fadeIn();
	}
	
	function displayError(message) {
		$('.alert').remove();
		$('#payment-form').after('<div class="warning alert alert-danger" style="display: none">' + message + '</div>');
		$('.warning').fadeIn();
		$('#button-confirm').attr('onclick', 'confirmOrder()').removeAttr('disabled');
		$('#card-select').removeAttr('disabled');
	}
	
	function deleteCard(token) {
		if (confirm('<?php echo $text_confirm; ?>')) {
			$.ajax({
				url: 'index.php?route=<?php echo $type; ?>/<?php echo $name; ?>/deleteCard&token=' + token,
				beforeSend: function() {
					displayWait();
				},
				success: function(success) {
					if (success) {
						$('.' + token + ', .alert').remove();
						alert('<?php echo $text_success; ?>');
					} else {
						displayError('<?php echo $error_card_token_not_found; ?>');
					}
					$('#button-confirm').attr('onclick', 'confirmOrder()').removeAttr('disabled');
					$('#card-select').removeAttr('disabled').change();
				}
			});
		}
	}
	
	function chargeCard(nonce, token) {
		$.ajax({
			type: 'POST',
			url: 'index.php?route=<?php echo $type; ?>/<?php echo $name; ?>/send',
			data: {nonce: nonce, token: token, store_card: $('#store-card').is(':checked'), device_data: $('#device_data').val()},
			dataType: 'json',
			beforeSend: function() {
				displayWait();
			},
			success: function(json) {
				if (json['error']) {
					displayError(json['error']);
				} else if (json['success']) {
					location = json['success'];
				}
			}
		});
	}
</script>

<script type="text/javascript">
	window.onBraintreeDataLoad = function() {
		BraintreeData.setup('<?php echo $settings[$settings['server_mode'] . '_merchant_id']; ?>', 'payment-form', BraintreeData.environments.production);
	};
</script>
<script src="https://js.braintreegateway.com/v1/braintree-data.js" async="true"></script>
