<div class="box">
	<div class="box-heading"><?php echo $heading_title; ?></div>
    <div class="box-content" >
		<?php if( !empty($blogs) ) { ?>
        <?php if(($position == 'content_top') || ($position == 'content_bottom') || ($position == 'content_bottom_half')) { ?>
    	<div class="items_wrapper carousel <?php echo $module; ?> pavcol<?php echo $cols;?>">
    	<?php } else { ?>
    	<div class="items_wrapper list">
    	<?php } ?>
			<?php foreach( $blogs as $key => $blog ) { $key = $key + 1;?>
					<div class="blog-item">

                            <?php if( $blog['thumb']  )  { ?>
							<div class="image zoom_image_container">
                            <a href="<?php echo $blog['link'];?>" title="<?php echo $blog['title'];?>"><img class="zoom_image" src="<?php echo $blog['thumb'];?>" alt="<?php echo $blog['title'];?>"/></a>
                            </div>
							<?php } ?>
                                
                            <div class="summary">
                            
                            <div class="left contrast_font">
                            <span class="date_added secondary_background">
							<span class="day"><?php echo date("d",strtotime($blog['created']));?></span>
							<span class="month"><?php echo date("M",strtotime($blog['created']));?></span>
							</span>
                            <a href="<?php echo $blog['link'];?>" title="<?php echo $blog['title'];?>"><span class="comment_count"><i class="fa fa-comments"></i> <?php echo $blog['comment_count'];?></span></a>
                            </div>
                            
                            <div class="right">                            
							<h4><a href="<?php echo $blog['link'];?>" title="<?php echo $blog['title'];?>"><?php echo $blog['title'];?></a></h4>
							<div class="description">
							<?php echo utf8_substr( $blog['description'],0, 150 );?>...
							</div>
                            </div>
                            
                            </div>
                                

                       </div>
			
			<?php } ?>
		</div>
		<?php } ?>
	</div>
 </div>
<?php if(($position == 'content_top') || ($position == 'content_bottom') || ($position == 'content_bottom_half')) { ?>
<script type="text/javascript">
$(document).ready(function() {
$('.items_wrapper.carousel.<?php echo $module; ?>').owlCarousel({
itemsCustom: [ [0, 1], [500, 2], [767, <?php echo $cols;?>]],
pagination: false,
navigation:true,
slideSpeed:500,
navigationText: [
"<div class='slide_arrow_prev'><i class='fa fa-angle-left'></i></div>",
"<div class='slide_arrow_next'><i class='fa fa-angle-right'></i></div>"]
}); });
</script>
<?php } ?>