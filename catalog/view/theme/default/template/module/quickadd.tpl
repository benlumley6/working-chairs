<style>

@media (min-width: 768px) {
#pb1
{
padding-left:7px!important;
padding-right:2px!important;

}
#pb .form-control
{
padding-left:2px;

}
#pb .col-sm-3 ,#pb .col-sm-4, #pb .col-sm-5,#pb .col-sm-12
{
padding-left:5px;
padding-right:3px;
}
#qcart td
{
padding-left:2px;
padding-right:2px;

}
#pf
{
padding-left:0px;
padding-right:0px;
}
#pf .col-sm-12 ,#pf .col-sm-6 ,#pf .col-sm-5 ,#pf .col-sm-4
{
padding-left:3px;
padding-right:3px;

}
}
</style>
<div class="panel panel-default" style="overflow1:hidden" >
  <div class="panel-heading">
        <h3 class="panel-title"><?php echo $heading_title?></h3>
      </div>
	  <div class='panel-body' id='pb' >
<div  id='quickshop'>

<div id='msgbar'></div>
 <div class='row'>
 <div class='col-md-6 col-sm-5'> <label class="control-label" for="input-name"><?php echo $text_product; ?></label>
    <input type="text" name="product_search"  id="input-product-search" class="form-control" />
 <input type='hidden' id='rpid' name='rpid'/>
 
 </div>
 <div class='col-md-3 col-sm-3'> <label class="control-label" for="input-name"><?php echo $text_qty; ?></label>
  <input type="text" name="qty" value="1" id='prqty' maxlength="3" class="form-control" />
</div>

 
  <div class='col-md-3 col-sm-3 ' style="padding-top:24px;">
  <label>&nbsp;</label>
 <a class='btn btn-success' id='btn-cart'><i class="fa fa-plus"></i></a>
 </div>

 </div><!--col-md-4 -->
 
 
 
 </div><!--row-->
 <div class='row'>
 <div class='col-md-12 col-sm-12'>
 <p></p>
 <div id='qcart'></div>
 
 </div>
 

 
 
 
</div>

</div><!--panelbody-->
<div class="panel-footer" id='pf' style="display:none">  <div class='row'><div class='col-md-6 col-sm-5'><p class="text-center"><a href="<?php echo $cart; ?>"><strong><i class="fa fa-shopping-cart"></i> <?php echo $text_cart; ?></strong></a></p></div>
<div class='col-md-6 col-sm-5'>
<p class="text-center">
<a href="<?php echo $checkout; ?>"><strong><i class="fa fa-share"></i> <?php echo $text_checkout; ?></strong></a></p></div></div>
</div>
</div><!--panel-->
<script type="text/javascript">



$('input[name=\'product_search\']').autocomplete({
minLength:3,
 change: function( event, ui ) {
 //alert("hi");
 	$('#rpid').val('');
 
 },
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=module/quickadd/autocomplete&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'product_search\']').val(item.label);
			$('#rpid').val(item.value);
			return false;
		/*
		$('#product-related' + item['value']).remove();
		
		$('#product-related').append('<div id="product-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_related[]" value="' + item['value'] + '" /></div>');	*/
	}	
});


</script>
<script type="text/javascript"><!--
$('#btn-cart').on('click', function() {
	
	qty=$('#prqty').val();
	if(qty*1==0)
	qty=1;
	product_id=$('#rpid').val();
	if(product_id*1==0)
	{
	alert('<?php echo $error_please_select_product?>');
	return ;
	
	
	}
	atc(product_id,qty);
	
});
function atc(product_id, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			success: function(json) {
			$('input[name=\'product_search\']').val('');
			$('#rpid').val('');
				$('.alert, .text-danger').remove();

				$('#cart > button').button('reset');

				if (json['redirect']) {
				alert("<?php echo $text_msg_options?>");
					location = json['redirect'];
				}

				if (json['success']) {
					$('#msgbar').html('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + '<?php echo $text_added_item;?>' + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					$('#cart-total').html(json['total']);

					

					$('#cart > ul').load('index.php?route=common/cart/info ul li');
						$('#qcart').load('index.php?route=module/quickadd/info');
				}
			}
		});
	}
	$(document).ready(function()
	{
	
		$('#qcart').load('index.php?route=module/quickadd/info');
	
	})
//--></script> 
