

<?php if (($style) == ('box')) { ?>

<div class="box newsletter boxed primary_background">

<div class="inner">
  <div class="box-content">
  <span class="heading"><?php echo $heading_title; ?></span>
  <p><?php echo $heading_welcome; ?></p>
          <form name="subscribe" id="subscribe<?php echo $module; ?>">
             <div <?php if(!$thickbox) { ?>style="display:none;"<?php } ?>>
             <input type="text" placeholder="<?php echo $entry_name; ?>" value="" name="subscribe_name" id="subscribe_name">
             </div>
             <div class="subscribe_form">
             <input type="text" placeholder="<?php echo $entry_email; ?>" value="" name="subscribe_email" id="subscribe_email">
             <a class="subscribe_icon" onclick="email_subscribe<?php echo $module; ?>()"><i class="fa fa-envelope"></i></a>
             </div>
			 <?php if($option_unsubscribe) { ?>
			 <a class="unsubscribe" onclick="email_unsubscribe<?php echo $module; ?>()"><span><?php echo $entry_unbutton; ?></span></a>
			 <?php } ?>    
          </form>
          </div>
	</div>
    </div>

<?php } else if (($style) == ('default')) { ?>

<div class="box newsletter">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
  <span class="contrast_font intro"><?php echo $heading_welcome; ?></span>
          <form name="subscribe" id="subscribe<?php echo $module; ?>">
             <div <?php if(!$thickbox) { ?>style="display:none;"<?php } ?>>
             <input type="text" placeholder="<?php echo $entry_name; ?>" value="" name="subscribe_name" id="subscribe_name">
             </div>
             <input type="text" placeholder="<?php echo $entry_email; ?>" value="" name="subscribe_email" id="subscribe_email">
             <a class="button" onclick="email_subscribe<?php echo $module; ?>()"><span><?php echo $entry_button; ?></span></a>
			 <?php if($option_unsubscribe) { ?>
			 <a class="button remove" onclick="email_unsubscribe<?php echo $module; ?>()"><span><?php echo $entry_unbutton; ?></span></a>
			 <?php } ?>    
          </form>
	</div>
</div>
<?php } ?>


<script type="text/javascript">
function email_subscribe<?php echo $module; ?>(){
	$.ajax({
			type: 'post',
			url: 'index.php?route=module/newslettersubscribe/subscribe',
			dataType: 'html',
            data:$("#subscribe<?php echo $module; ?>").serialize(),
			success: function (html) {
				eval(html);
			}}); 
	
	$('html, body').animate({ scrollTop: 0 }, 'slow'); 
	
}
function email_unsubscribe<?php echo $module; ?>(){
	$.ajax({
			type: 'post',
			url: 'index.php?route=module/newslettersubscribe/unsubscribe',
			dataType: 'html',
            data:$("#subscribe<?php echo $module; ?>").serialize(),
			success: function (html) {
				eval(html);
			}}); 
	
	$('html, body').animate({ scrollTop: 0 }, 'slow'); 
	
}
</script>

