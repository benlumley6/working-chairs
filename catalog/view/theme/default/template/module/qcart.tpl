<div>
  

    <?php if ($products) { ?>
    
      <table class="table table-striped table-responsive">
        <?php foreach ($products as $product) { ?>
        <tr>
        
          <td class="text-left"><?php echo $product['name']; ?>
          x <?php echo $product['quantity']; ?></td>
          <td class="text-right"><?php echo $product['total']; ?></td>
          <td class="text-center"><button type="button" onclick="rem('<?php echo $product['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button></td>
        </tr>
        <?php } ?>
        
      </table>
   
      <div>
       
      </div>
     <script type="text/javascript">
	  
	  $('#pf').show();
	  
	  </script>
    <?php } else { ?>
   
      <p class="text-center"><?php echo $text_empty; ?></p>
	  <script type="text/javascript">
	  
	  $('#pf').hide();
	  
	  </script>
    
    <?php } ?>
  
</div>
<script>

function rem(key)
{

cart.remove(key);
$('#qcart').load('index.php?route=module/quickadd/info');
}

</script>
