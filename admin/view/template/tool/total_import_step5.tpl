<?php
#####################################################################################
#  Module TOTAL IMPORT PRO for Opencart 1.5.x From HostJars opencart.hostjars.com 	#
#####################################################################################
?>
<?php echo $header; ?>
<form action="<?php echo $action; ?>" method="post" name="settings_form" enctype="multipart/form-data" id="import_form">
<input type='hidden' value='import_step5' name='step'/>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<script type="text/javascript">
function enableRange(check) {
	if(check) {
		$("#import_range_start").removeAttr("disabled");
		$("#import_range_end").removeAttr("disabled");
	} else {
		 $("#import_range_start").attr("disabled",true);
		 $("#import_range_end").attr("disabled",true);
	}
}
$(document).ready(function() {
	<?php if (isset($import_range) && $import_range == 'partial') echo 'enableRange(true);'; ?>
	<?php if (isset($import_range) && $import_range == 'all') echo 'enableRange(false);'; ?>
});
</script>
<style>
.progress {
height: 20px;
margin-bottom: 20px;
overflow: hidden;
background-color: #f5f5f5;
border-radius: 4px;
-webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
}
.progress-bar {
float: left;
width: 0;
height: 100%;
font-size: 12px;
line-height: 20px;
color: #fff;
text-align: center;
background-color: #428bca;
-webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
-webkit-transition: width .6s ease;
-o-transition: width .6s ease;
transition: width .6s ease;
}
</style>
<div class="box">
  <div class="heading">
    <h1><img src='view/image/feed.png' /><?php echo $heading_title; ?>&nbsp;(<a title='<?php echo $text_documentation; ?>' href='<?php echo $help_link; ?>'><?php echo $text_documentation; ?></a>)</h1>
	<div class="buttons">
		<?php echo $text_save_profile; ?><input type="text" name="save_settings_name" value="">
		<a onclick="$('#import_form').submit();" id="btn-import" class="button"><span><?php echo $button_import; ?></span></a>
		<a onclick="javascript:saveSettings();return false;" class="button"><span><?php echo $button_save; ?></span></a>
		<a href="<?php echo $cancel; ?>" class="button"><span><?php echo $button_cancel; ?></span></a>
	</div>
  </div>
  <div class="content">
	<div id="tabs" class="htabs"><!-- <a href="#tab_fetch">Step 1: Fetch Feed</a><a href="#tab_adjust"><?php echo $tab_adjust; ?></a><a href="#tab_global"><?php echo $tab_global; ?></a><a href="#tab_mapping"><?php echo $tab_mapping; ?></a> --><a href="#tab_import"><?php echo $tab_import; ?></a></div>
	    <div id="tab_import">
        <table class="form">
            <tr class="reset">
            <td></td>
                <td>
              <div class="form-group" id="progress-items" style="display:none;">
                            <div class="col-sm-2 control-label">
                            </div>
                            <div class="col-sm-10">
                                <div class="progress">
                                    <div id="progress-bar" class="progress-bar" style="width: 0%;"></div>
                                </div>
                                <div id="progress-text">
                                    <span id="import-status">Importing...</span> Updated: <span id="prod_update">0</span> Added: <span id="prod_add">0</span>
                                </div>
                            </div>
                        </div>
                    </td>
            </tr>
            <tr>
                <td>Select tables to reset <a href="http://helpdesk.hostjars.com/entries/22032291-step-5-import" target="_blank" alt="Profiles"><img class="info_image" src="view/image/information.png" title="Details on Resetting tables"></a></td>
                <td>
                <div class="scrollbox">
                    <div class="even">
                        <input type="checkbox" name="reset_products" id="reset_products" <?php if (isset($reset_products)) echo 'checked="1"'?>>
                        <label for="reset_products"><?php echo $table_products; ?></label>
                    </div>
                    <div class="odd">
                        <input type="checkbox" name="reset_categories" id="reset_categories" <?php if (isset($reset_categories)) echo 'checked="1"'?>>
                        <label for="reset_categories"><?php echo $table_categories; ?></label>
                    </div>
                    <div class="even">
                        <input type="checkbox" name="reset_manufacturers" id="reset_manufacturers" <?php if (isset($reset_manufacturers)) echo 'checked="1"'?>>
                        <label for="reset_products"><?php echo $table_manufacturers; ?></label>
                    </div>
                    <div class="odd">
                        <input type="checkbox" name="reset_attributes" id="reset_attributes" <?php if (isset($reset_attributes)) echo 'checked="1"'?>>
                        <label for="reset_attributes"><?php echo $table_attributes; ?></label>
                    </div>
                    <div class="even">
                        <input type="checkbox" name="reset_options" id="reset_options" <?php if (isset($reset_options)) echo 'checked="1"'?>>
                        <label for="reset_options"><?php echo $table_categories; ?></label>
                    </div>
                    <div class="odd">
                        <input type="checkbox" name="reset_downloads" id="reset_downloads" <?php if (isset($reset_downloads)) echo 'checked="1"'?>>
                        <label for="reset_downloads"><?php echo $table_downloads; ?></label>
                    </div>
                    <div class="even">
                        <input type="checkbox" name="reset_filters" id="reset_filters" <?php if (isset($reset_filters)) echo 'checked="1"'?>>
                        <label for="reset_filters"><?php echo $table_filters; ?></label>
                    </div>
                </div>
                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);">Select All</a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);">Unselect All</a>
                </td>
            </tr>

        	<!-- New Items -->
        	<tr class="non_reset">
        		<td><?php echo $entry_new_items; ?></td>
        		<td>
	        		<select name="new_items">
	        			<option value="add"><?php echo $entry_add; ?></option>
	        			<option value="skip" <?php if (isset($new_items) && $new_items == 'skip') echo 'selected="true"'; ?>><?php echo $entry_skip; ?></option>
	        		</select>
        		</td>
        	</tr>
        	<!-- Existing Items -->
        	<tr class="non_reset">
        		<td><?php echo $entry_existing_items; ?></td>
        		<td>
        			<select name="existing_items">
        				<option value="update"><?php echo $entry_update; ?></option>
        				<option value="skip" <?php if (isset($existing_items) && $existing_items == 'skip') echo 'selected="true"'; ?>><?php echo $entry_skip; ?></option>
        			</select>
        		</td>
        	</tr>
        	<!-- Identify Existing Items -->
        	<tr class="non_reset">
        		<td><?php echo $text_identify_existing; ?></td>
        		<td>
					<select name="update_field">
						<option value="model"><?php echo $text_field_model; ?></option>
						<option value="sku" <?php if (isset($update_field) && $update_field == 'sku') echo 'selected="true"'; ?>><?php echo $text_field_sku; ?></option>
						<option value="upc" <?php if (isset($update_field) && $update_field == 'upc') echo 'selected="true"'; ?>><?php echo $text_field_upc; ?></option>
						<option value="ean" <?php if (isset($update_field) && $update_field == 'ean') echo 'selected="true"'; ?>><?php echo $text_field_ean; ?></option>
						<option value="jan" <?php if (isset($update_field) && $update_field == 'jan') echo 'selected="true"'; ?>><?php echo $text_field_jan; ?></option>
						<option value="isbn" <?php if (isset($update_field) && $update_field == 'isbn') echo 'selected="true"'; ?>><?php echo $text_field_isbn; ?></option>
						<option value="mpn" <?php if (isset($update_field) && $update_field == 'mpn') echo 'selected="true"'; ?>><?php echo $text_field_mpn; ?></option>
					</select>
				</td>
			</tr>
        	<!-- Items in store, not in feed -->
        	<tr class="non_reset">
        		<td><?php echo $entry_delete_diff; ?></td>
        		<td>
        			<select name="delete_diff">
        				<option value="ignore"><?php echo $entry_ignore; ?></option>
        				<option value="delete" <?php if (isset($delete_diff) && $delete_diff == 'delete') echo 'selected="true"'; ?>><?php echo $entry_delete; ?></option>
        				<option value="disable" <?php if (isset($delete_diff) && $delete_diff == 'disable') echo 'selected="true"'; ?>><?php echo $entry_disable; ?></option>
        				<option value="zero_quantity" <?php if (isset($delete_diff) && $delete_diff == 'zero_quantity') echo 'selected="true"'; ?>><?php echo $entry_zero_quantity; ?></option>
        			</select>
        		</td>
        	</tr>
			<!-- Product Range to Import -->
        	<tr class="non_reset">
        		<td>
        			<?php echo $entry_import_range; ?>
        			<span class="help"><?php echo $entry_import_range_help; ?></span>
        		</td>
        		<td>
					<input type="radio" id="import_range_all" class="option_radio" name="import_range" onclick='javascript:enableRange(false);' value="all" <?php if ((isset($import_range) && $import_range == 'all') || !isset($import_range)) echo 'checked="checked"'; ?>><label for='import_range_all'><?php echo $entry_all; ?></label>
					<input type="radio" id="import_range_partial" class="option_radio" onclick='javascript:enableRange(true);' name="import_range" value="partial" <?php if (isset($import_range) && $import_range == 'partial') echo 'checked="checked"'; ?>><label for='import_range_partial'><?php echo $entry_range; ?>&nbsp;-&nbsp;<span id="display_import_range"><?php echo $entry_from; ?> <input type="text" id="import_range_start" name="import_range_start" size="6" value="<?php if(!isset($import_range_start))  echo '1'; if(isset($import_range_start)) echo $import_range_start; ?>"> <?php echo $entry_to; ?> <input type="text" id="import_range_end" name="import_range_end" size="6" value="<?php if(!isset($import_range_end)) echo '1000'; if(isset($import_range_end)) echo $import_range_end; ?>"></span></label>
				</td>
			</tr>
        </table>
       </div>
  </div>
</div><script type="text/javascript"><!--
$('#tabs a').tabs();

function saveSettings() {
	var data = $('#import_form').serialize();
	var url = 'index.php?route=tool/total_import/saveSettings&token=<?php echo $token ?>';
	$.ajax({
		type: "POST",
		url: url,
		data: data,
		success: function(result) {
			addSave(result);
		}
	});
}

function addSave(result) {
	$('.success').remove();
	$('.warning').hide();
	$('.breadcrumb').append('<div class="success" style="margin-top:15px;">'+result+'</div>');
}

if (<?php echo $remote_images ?>) {
    var_step_amount = 10; //10
} else {
    var_step_amount = 50; //50
}

function next(cur_start, import_end, firstRun) {
    firstRun = typeof firstRun !== 'undefined' ? 1 : 0;
    $.ajax({
        url: 'index.php?route=tool/total_import/step5_ajax&token=<?php echo $token; ?>',
        type: 'post',
        dataType: 'json',
        data: $('#import_form').serialize() + '&START='+ cur_start +'&END='+ Math.min((cur_start + var_step_amount), import_end) + (firstRun ? '&FIRSTRUN=1' : ''),
        success: function(json) {
            $('#prod_update').text(json['updated'] + parseInt($('#prod_update').text()));
            $('#prod_add').text(json['added'] + parseInt($('#prod_add').text()));

            // Draw Progress bars
            if ($('#import_range_all').is(':checked')) {
                $('#progress-bar').css('width', ((cur_start / json['maxProducts']) * 100) + '%');
            } else {
                range_difference = +$('#import_range_end').val() - +$('#import_range_start').val();
                num_imported = cur_start - +$('#import_range_start').val();
                $('#progress-bar').css('width', ((num_imported / range_difference) * 100) + '%');
            }

            // Recusrive call or end
            if ((cur_start + var_step_amount) < import_end) {
                next(cur_start + var_step_amount + 1, import_end);
            } else {
                $('#import-status').text('Complete!');
                $('#progress-bar').css('width', 100 + '%').addClass('progress-bar-success');
                $('#btn-import').prop('disabled', false);
                $("#import_range_start").removeAttr("disabled", "disabled");
                $("#import_range_end").removeAttr("disabled", "disabled");
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            //console.log(thrownError);
        }
    });
}

$('#import_form').submit(function(event) {
    event.preventDefault();
    var isFullImport = $('#import_range_all').is(':checked');
    if (isFullImport) {
        var_start = 1
        var_end = -1;
        $.ajax({
            url: 'index.php?route=tool/total_import/getNumItemsInFeed&token=<?php echo $token; ?>',
            type: 'get',
            async: false,
            success: function(data) {
                var_end = parseInt(data);
                console.log("DATA",var_end);
            }
        });
    } else {
        var_start = +$('#import_range_start').val();
        var_end = +$('#import_range_end').val();
    }
    console.log("var_end:", var_end);
    $('#progress-items').slideDown();
    $('#btn-import').prop('disabled', true);
    $('#import-status').text('Importing...');
    $('#prod_update').text(0);
    $('#prod_add').text(0);
    $('#progress-bar').css('width', 0 + '%').removeClass('progress-bar-success');
    $("#import_range_start").attr("disabled", "disabled");
    $("#import_range_end").attr("disabled", "disabled");
    next(Math.max(var_start, 1), var_end, true);
});
//--></script>
</form>
<?php echo $footer; ?>