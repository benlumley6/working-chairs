//==============================================================================
// Braintree Payment Gateway v210.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================

function getQueryVariable(variable) {
	var vars = window.location.search.substring(1).split('&');
	for (i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		if (pair[0] == variable) return pair[1];
	}
	return false;
}

function braintreeSubmit(element, charge_id) {
	element.after('<span id="please-wait" style="font-size: 11px"> Please wait...</span>');
	$.get('index.php?route=payment/braintree/submit&charge_id=' + charge_id + '&token=' + getQueryVariable('token'),
		function(error) {
			$('#please-wait').remove();
			if (error) {
				alert(error);
			} else {
				element.prev().html('Yes');
				element.remove();
			}
		}
	);
}

function braintreeRefund(element, charge_id) {
	if (confirm('This operation cannot be undone. Continue?')) {
		element.after('<span id="please-wait" style="font-size: 11px"> Please wait...</span>');
		$.get('index.php?route=payment/braintree/refund&charge_id=' + charge_id + '&order_id=' + getQueryVariable('order_id') + '&token=' + getQueryVariable('token'),
			function(error) {
				if (error) {
					alert(error);
					$('#please-wait').remove();
				} else {
					alert('Success!');
					setTimeout(function(){
						$('#history').load('index.php?route=sale/order/history&token=' + getQueryVariable('token') + '&order_id=' + getQueryVariable('order_id'));
						$('#please-wait').remove();
					}, 2000);
				}
			}
		);
	}
}