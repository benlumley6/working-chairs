<?php
//==============================================================================
// Braintree Payment Gateway v210.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================

$version = 'v210.1';

//------------------------------------------------------------------------------
// Heading
//------------------------------------------------------------------------------
$_['heading_title']						= 'Braintree';
$_['text_braintree']					= '<a target="_blank" href="https://www.braintreepayments.com/contact-us?source=ClearThinking"><img src="//braintree-badges.s3.amazonaws.com/07.png" alt="Braintree" title="Braintree" /></a>';

//------------------------------------------------------------------------------
// Extension Settings
//------------------------------------------------------------------------------
$_['tab_extension_settings']			= 'Extension Settings';
$_['heading_extension_settings']		= 'Extension Settings';

$_['entry_status']						= 'Status: <div class="help-text">Set the status for the extension as a whole.</div>';
$_['entry_sort_order']					= 'Sort Order: <div class="help-text">Enter the sort order for the extension, relative to other payment methods.</div>';
$_['entry_title']						= 'Title: <div class="help-text">Enter the title for the payment method displayed to the customer. HTML is supported.</div>';
$_['entry_input_styling']				= 'Credit Card Input Styling: <div class="help-text">Choose how the credit card inputs are styled. Note: Braintree&apos;s <a target="_blank" href="https://www.braintreepayments.com/features/drop-in">Drop-in UI</a> cannot be styled beyond its default look, does not perform address verification, and is English-only.</div>';
$_['entry_button_text']					= 'Button Text: <div class="help-text">Enter the text for the order confirmation button.</div>';
$_['entry_button_class']				= 'Button Class: <div class="help-text">Enter the CSS class for buttons in your theme.</div>';
$_['entry_button_styling']				= 'Button Styling: <div class="help-text">Optionally enter extra CSS styling for the button.</div>';

$_['text_standard_inputs']				= 'Standard Inputs';
$_['text_paypal_plus_standard_inputs']	= 'PayPal + Standard Inputs';
$_['text_braintree_dropin_ui']			= 'Braintree Drop-in UI';

//------------------------------------------------------------------------------
// Restrictions
//------------------------------------------------------------------------------
$_['tab_restrictions']					= 'Restrictions';
$_['heading_restrictions']				= 'Restrictions';

$_['entry_order_status_id']				= 'Order Status: <div class="help-text">Select the order status set for successful payments.</div>';
$_['entry_min_total']					= 'Minimum Total: <div class="help-text">Enter the minimum order total that must be reached before this payment method becomes active. Leave blank to have no restriction.</div>';
$_['entry_max_total']					= 'Maximum Total: <div class="help-text">Enter the maximum order total that can be reached before this payment method becomes active. Leave blank to have no restriction.</div>';
$_['entry_stores']						= 'Store(s): <div class="help-text">Select the stores that can use this payment method.</div>';
$_['entry_geo_zones']					= 'Geo Zone(s): <div class="help-text">Select the geo zones that can use this payment method. The "Everywhere Else" checkbox applies to any locations not within a geo zone.</div>';
$_['entry_customer_groups']				= 'Customer Group(s): <div class="help-text">Select the customer groups that can use this payment method. The "Guests" checkbox applies to all customers not logged in to an account.</div>';
$_['entry_currencies']					= 'Currencies: <div class="help-text">Select the currencies that are eligible for this payment method.</div>';

$_['text_everywhere_else']				= '<em>Everywhere Else</em>';
$_['text_guests']						= '<em>Guests</em>';

//------------------------------------------------------------------------------
// Braintree Settings
//------------------------------------------------------------------------------
$_['tab_braintree_settings']			= 'Braintree Settings';
$_['heading_braintree_settings']		= 'Braintree Settings';

$_['entry_server_mode']					= 'Server Mode: <div class="help-text">Use "Sandbox" to test payments through your Braintree sandbox. For more info on how to test, visit <a href="https://developers.braintreepayments.com/reference/general/testing" target="_blank">this page</a>. Use "Production (Live)" when you&apos;re ready to accept payments.</div>';
$_['text_sandbox']						= 'Sandbox';
$_['text_production']					= 'Production (Live)';

$_['entry_charge_mode']					= 'Charge Mode: <div class="help-text">Choose whether to authorize payments and manually settle them later, or to both authorize and submit for settlement (i.e. fully charge) payments when orders are placed. You can settle a payment that is only Authorized by using the link provided in the History tab for the order.</div>';
$_['text_authorize']					= 'Authorize';
$_['text_submit_for_settlement']		= 'Submit for Settlement';

$_['entry_store_card']					= 'Store Card Details in Vault: <div class="help-text">Select whether to store card details in your Braintree vault. The credit card used will be attached to this customer, allowing you to charge them again in the future within Braintree.</div>';
$_['text_never']						= 'Never';
$_['text_customers_choice']				= 'Customer&apos;s choice';
$_['text_always']						= 'Always';

$_['entry_store_billing']				= 'Store Billing Address in Vault: <div class="help-text">Choose whether the customer&apos;s billing address is stored along with their card information.</div>';
$_['entry_store_shipping']				= 'Store Shipping Address in Vault: <div class="help-text">Choose whether the customer&apos;s shipping address is stored along with their card information.</div>';

$_['entry_allow_stored_cards']			= 'Allow Customers to Use Stored Cards: <div class="help-text">Choose whether customers that have cards stored in Braintree will be able to use those cards for future purchases in your store, without having to re-enter the information.</div>';

// Production (Live) API Keys
$_['heading_production_api_keys']		= 'Production (Live) API Keys';
$_['help_production_api_keys']			= 'API Keys can be found in your <b>Braintree</b> admin panel under Account > My user > Authorization > API Keys';

$_['entry_production_public_key']		= 'Public Key:';
$_['entry_production_private_key']		= 'Private Key:';
$_['entry_production_merchant_id']		= 'Merchant ID:';

$_['help_merchant_account_id']			= 'To charge orders in a specific currency when it is selected by the customer, enter the appropriate Merchant Account ID below. These are different than your Merchant ID, and can be found in your Braintree admin panel under Settings > Processing. Leave the fields blank to have the order converted and charged in your default currency.';

$_['text_merchant_account_id']			= ' Merchant Account ID:';

// Sandbox API Keys
$_['heading_sandbox_api_keys']			= 'Sandbox API Keys';
$_['help_sandbox_api_keys']				= 'API Keys can be found in your <b>Sandbox</b> admin panel under Account > My user > Authorization > API Keys';

$_['entry_sandbox_public_key']			= 'Public Key:';
$_['entry_sandbox_private_key']			= 'Private Key:';
$_['entry_sandbox_merchant_id']			= 'Merchant ID:';

//------------------------------------------------------------------------------
// Subscription Products
//------------------------------------------------------------------------------
$_['tab_subscription_products']			= 'Subscription Products';
$_['heading_subscription_products']		= 'Subscription Products';
$_['help_subscription_products']		= 'Subscription products will subscribe the customer to the associated Braintree plan when they are purchased. You can associate a product with a plan by entering the Braintree plan ID in the "Location" field for the product.<br /><br />If the subscription is not set to be charged immediately (i.e. it has a trial period), the amount of the subscription will be taken off their original order, and a new order will be created when the subscription is actually charged to their card. Any time Braintree charges the subscription in the future, a corresponding order will be created in OpenCart.';

$_['entry_subscriptions']				= 'Enable Subscription Products:';
$_['entry_prevent_guests']				= 'Prevent Guests From Purchasing: <div class="help-text">If set to "Yes", only customers with accounts in OpenCart will be allowed to checkout if a subscription product is in the cart.</div>';

$_['entry_webhook_url']					= 'Webhook URL: <div class="help-text">If using subscriptions, copy and paste this URL into your Braintree account, by choosing Settings > Webhooks > New. If you haven&apos;t already, you must first enable webhooks in:<br />Settings > Users and Roles > Manage Roles > Edit > Manage Webhooks.</div>';
$_['help_webhook_url']					= '<div class="help-text">If you change your store&apos;s Encryption Key in System > Settings > Server, this URL will change, so remember to add the new webhook URL in Braintree.</div>';

$_['entry_subscription_shipping']		= 'Add Shipping and Taxes: <div class="help-text">Choose whether shipping and taxes will be added to the price of future subscription charges. The shipping option with the same name selected by the customer will be used for each subscription product. The new price (subscription price + shipping + taxes) will override the default plan price for as long as the customer is subscribed.</div>';

$_['entry_prevent_guests_from']			= 'Prevent Guests From Purchasing: <div class="help-text">Choose whether to allow only logged-in customers to checkout with subscription products in their cart.</div>';

$_['entry_current_subscriptions']		= 'Current Subscription Products: <div class="help-text">Products with mismatching prices are highlighted. The OpenCart product price will override the plan price if different, so you can set subscription products on Special in OpenCart and it will charge the correct Special price.<br /><br />Note: only plans for your Server Mode will be listed. You are currently set to <b>[server_mode]</b> mode.</div>';

$_['text_thead_opencart']				= 'OpenCart';
$_['text_thead_braintree']				= 'Braintree';
$_['text_product_name']					= 'Product Name';
$_['text_product_price']				= 'Product Price';
$_['text_location_plan_id']				= 'Location / Plan ID';
$_['text_plan_name']					= 'Plan Name';
$_['text_plan_interval']				= 'Plan Interval';
$_['text_plan_charge']					= 'Plan Charge';
$_['text_no_subscription_products']		= 'No Subscription Products';
$_['text_create_one_by_entering']		= 'Create one by entering the Braintree plan ID in the "Location" field for the product';

//------------------------------------------------------------------------------
// Standard Text
//------------------------------------------------------------------------------
$_['copyright']							= '<hr /><div class="text-center" style="margin: 15px">' . $_['heading_title'] . ' (' . $version . ') &copy; <a target="_blank" href="http://www.getclearthinking.com">Clear Thinking, LLC</a></div>';

$_['standard_autosaving_enabled']		= 'Auto-Saving Enabled';
$_['standard_confirm']					= 'This operation cannot be undone. Continue?';
$_['standard_error']					= '<strong>Error:</strong> You do not have permission to modify ' . $_['heading_title'] . '!';
$_['standard_warning']					= '<strong>Warning:</strong> The number of settings is close to your <code>max_input_vars</code> server value. You should enable auto-saving to avoid losing any data.';
$_['standard_please_wait']				= 'Please wait...';
$_['standard_saving']					= 'Saving...';
$_['standard_saved']					= 'Saved!';
$_['standard_select']					= '--- Select ---';
$_['standard_success']					= 'Success!';

$_['standard_module']					= 'Modules';
$_['standard_shipping']					= 'Shipping';
$_['standard_payment']					= 'Payments';
$_['standard_total']					= 'Order Totals';
$_['standard_feed']						= 'Feeds';
?>