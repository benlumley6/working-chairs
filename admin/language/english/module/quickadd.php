<?php
// Heading
$_['heading_title']    = 'Quickadd';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Quickadd module!';
$_['text_edit']        = 'Edit Quickadd Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Quickadd module!';