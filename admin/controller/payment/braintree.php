<?php
//==============================================================================
// Braintree Payment Gateway v210.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================

class ControllerPaymentBraintree extends Controller { 
	private $type = 'payment';
	private $name = 'braintree';
	
	public function index() {
		$data = array(
			'type'				=> $this->type,
			'name'				=> $this->name,
			'autobackup'		=> false,
			'save_type'			=> 'keepediting',
			'token'				=> $this->session->data['token'],
			'permission'		=> $this->user->hasPermission('modify', $this->type . '/' . $this->name),
			'exit'				=> $this->url->link('extension/' . $this->type . '&token=' . $this->session->data['token'], '', 'SSL'),
		);
		
		// Convert old settings
		$old_settings_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `key` = '" . $this->name . "_data'");
		if ($old_settings_query->num_rows) {
			$old_settings = unserialize($old_settings_query->row['value']);
			foreach ($old_settings as $key => $value) {
				// extension-specific
				if ($key == 'transaction_method') $key = 'charge_mode';
				// end
				if (is_array($value)) {
					if (is_int(key($value))) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET `" . (version_compare(VERSION, '2.0.1') < 0 ? 'group' : 'code') . "` = '" . $this->name . "', `key` = '" . $this->name . "_" . $key . "', `value` = '" . implode(';', $value) . "'");
					} else {
						foreach ($value as $value_key => $value_value) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET `" . (version_compare(VERSION, '2.0.1') < 0 ? 'group' : 'code') . "` = '" . $this->name . "', `key` = '" . $this->name . "_" . $key . "_" . $value_key . "', `value` = '" . $value_value . "'");
						}
					}
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET `" . (version_compare(VERSION, '2.0.1') < 0 ? 'group' : 'code') . "` = '" . $this->name . "', `key` = '" . $this->name . "_" . $key . "', `value` = '" . $value . "'");
				}
			}
			$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `key` = '" . $this->name . "_data'");
		}
		
		$this->loadSettings($data);
		
		//------------------------------------------------------------------------------
		// Data Arrays
		//------------------------------------------------------------------------------
		$this->load->model('localisation/language');
		$data['language_array'] = array($this->config->get('config_language') => '');
		$data['language_flags'] = array();
		foreach ($this->model_localisation_language->getLanguages() as $language) {
			$data['language_array'][$language['code']] = $language['name'];
			$data['language_flags'][$language['code']] = $language['image'];
		}
		
		$data['order_status_array'] = array();
		$this->load->model('localisation/order_status');
		foreach ($this->model_localisation_order_status->getOrderStatuses() as $order_status) {
			$data['order_status_array'][$order_status['order_status_id']] = $order_status['name'];
		}
		
		$data['customer_group_array'] = array(0 => $data['text_guests']);
		$this->load->model((version_compare(VERSION, '2.1', '<') ? 'sale' : 'customer') . '/customer_group');
		foreach ($this->{'model_' . (version_compare(VERSION, '2.1', '<') ? 'sale' : 'customer') . '_customer_group'}->getCustomerGroups() as $customer_group) {
			$data['customer_group_array'][$customer_group['customer_group_id']] = $customer_group['name'];
		}
		
		$data['geo_zone_array'] = array(0 => $data['text_everywhere_else']);
		$this->load->model('localisation/geo_zone');
		foreach ($this->model_localisation_geo_zone->getGeoZones() as $geo_zone) {
			$data['geo_zone_array'][$geo_zone['geo_zone_id']] = $geo_zone['name'];
		}
		
		$data['store_array'] = array(0 => $this->config->get('config_name'));
		$store_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store ORDER BY name");
		foreach ($store_query->rows as $store) {
			$data['store_array'][$store['store_id']] = $store['name'];
		}
		
		$data['currency_array'] = array($this->config->get('config_currency') => '');
		$this->load->model('localisation/currency');
		foreach ($this->model_localisation_currency->getCurrencies() as $currency) {
			$data['currency_array'][$currency['code']] = $currency['code'];
		}
		
		//------------------------------------------------------------------------------
		// Extensions Settings
		//------------------------------------------------------------------------------
		$data['settings'] = array();
		
		$data['settings'][] = array(
			'type'		=> 'tabs',
			'tabs'		=> array('extension_settings', 'restrictions', 'braintree_settings', 'subscription_products'),
		);
		$data['settings'][] = array(
			'key'		=> 'extension_settings',
			'type'		=> 'heading',
		);
		$data['settings'][] = array(
			'key'		=> 'status',
			'type'		=> 'select',
			'options'	=> array('0' => $data['text_disabled'], '1' => $data['text_enabled']),
		);
		$data['settings'][] = array(
			'key'		=> 'sort_order',
			'type'		=> 'text',
			'default'	=> '1',
			'attributes'=> array('style' => 'width: 50px !important'),
		);
		$data['settings'][] = array(
			'key'		=> 'title',
			'type'		=> 'multilingual_text',
			'default'	=> 'Credit / Debit Card',
		);
		$data['settings'][] = array(
			'key'		=> 'input_styling',
			'type'		=> 'select',
			'options'	=> array(
				'standard'	=> $data['text_standard_inputs'],
				'paypal'	=> $data['text_paypal_plus_standard_inputs'],
				'dropin'	=> $data['text_braintree_dropin_ui'],
			),
		);
		$data['settings'][] = array(
			'key'		=> 'button_text',
			'type'		=> 'multilingual_text',
			'default'	=> 'Confirm Order',
		);
		$data['settings'][] = array(
			'key'		=> 'button_class',
			'type'		=> 'text',
			'default'	=> (version_compare(VERSION, '2.0') < 0) ? 'button' : 'btn btn-primary',
		);
		$data['settings'][] = array(
			'key'		=> 'button_styling',
			'type'		=> 'text',
		);

		//------------------------------------------------------------------------------
		// Restrictions
		//------------------------------------------------------------------------------
		$data['settings'][] = array(
			'key'		=> 'restrictions',
			'type'		=> 'tab',
		);
		$data['settings'][] = array(
			'key'		=> 'restrictions',
			'type'		=> 'heading',
		);
		$complete_status = $this->config->get('config_complete_status');
		$data['settings'][] = array(
			'key'		=> 'order_status_id',
			'type'		=> 'select',
			'options'	=> $data['order_status_array'],
			'default'	=> is_array($complete_status) ? reset($complete_status) : $complete_status,
		);
		$data['settings'][] = array(
			'key'		=> 'min_total',
			'type'		=> 'text',
			'attributes'=> array('style' => 'width: 50px !important'),
		);
		$data['settings'][] = array(
			'key'		=> 'max_total',
			'type'		=> 'text',
			'attributes'=> array('style' => 'width: 50px !important'),
		);
		$data['settings'][] = array(
			'key'		=> 'stores',
			'type'		=> 'checkboxes',
			'options'	=> $data['store_array'],
			'default'	=> array_keys($data['store_array']),
		);
		$data['settings'][] = array(
			'key'		=> 'geo_zones',
			'type'		=> 'checkboxes',
			'options'	=> $data['geo_zone_array'],
			'default'	=> array_keys($data['geo_zone_array']),
		);
		$data['settings'][] = array(
			'key'		=> 'customer_groups',
			'type'		=> 'checkboxes',
			'options'	=> $data['customer_group_array'],
			'default'	=> array_keys($data['customer_group_array']),
		);
		$data['settings'][] = array(
			'key'		=> 'currencies',
			'type'		=> 'checkboxes',
			'options'	=> $data['currency_array'],
			'default'	=> array_keys($data['currency_array']),
		);
		
		//------------------------------------------------------------------------------
		// Braintree Settings
		//------------------------------------------------------------------------------
		$data['settings'][] = array(
			'key'		=> 'braintree_settings',
			'type'		=> 'tab',
		);
		$data['settings'][] = array(
			'key'		=> 'braintree_settings',
			'type'		=> 'heading',
		);
		$data['settings'][] = array(
			'key'		=> 'server_mode',
			'type'		=> 'select',
			'options'	=> array('sandbox' => $data['text_sandbox'], 'production' => $data['text_production']),
		);
		$data['settings'][] = array(
			'key'		=> 'charge_mode',
			'type'		=> 'select',
			'options'	=> array('authorize' => $data['text_authorize'], 'submit' => $data['text_submit_for_settlement']),
		);
		$data['settings'][] = array(
			'key'		=> 'store_card',
			'type'		=> 'select',
			'options'	=> array('never' => $data['text_never'], 'choice' => $data['text_customers_choice'], 'always' => $data['text_always']),
		);
		$data['settings'][] = array(
			'key'		=> 'store_billing',
			'type'		=> 'select',
			'options'	=> array('0' => $data['text_no'], '1' => $data['text_yes']),
		);
		$data['settings'][] = array(
			'key'		=> 'store_shipping',
			'type'		=> 'select',
			'options'	=> array('0' => $data['text_no'], '1' => $data['text_yes']),
		);
		$data['settings'][] = array(
			'key'		=> 'allow_stored_cards',
			'type'		=> 'select',
			'options'	=> array('0' => $data['text_no'], '1' => $data['text_yes']),
		);
		
		// API Keys
		foreach (array('production', 'sandbox') as $server_mode) {
			$data['settings'][] = array(
				'key'		=> $server_mode . '_api_keys',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'title'		=> '',
				'type'		=> 'html',
				'content'	=> '<div class="text-info">' . $data['help_' . $server_mode . '_api_keys'] . '</div>',
			);
			$data['settings'][] = array(
				'key'		=> $server_mode . '_merchant_id',
				'type'		=> 'text',
				'attributes'=> array('onchange' => '$(this).val($(this).val().trim())'),
			);
			$data['settings'][] = array(
				'key'		=> $server_mode . '_public_key',
				'type'		=> 'text',
				'attributes'=> array('onchange' => '$(this).val($(this).val().trim())'),
			);
			$data['settings'][] = array(
				'key'		=> $server_mode . '_private_key',
				'type'		=> 'text',
				'attributes'=> array('onchange' => '$(this).val($(this).val().trim())'),
			);
			$data['settings'][] = array(
				'title'		=> '',
				'type'		=> 'html',
				'content'	=> '<div class="well" style="margin: 0">' . $data['help_merchant_account_id'] . '</div>',
			);
			foreach ($data['currency_array'] as $currency) {
				$data['settings'][] = array(
					'key'		=> $currency . '_' . $server_mode . '_merchant_account',
					'title'		=> $currency . $data['text_merchant_account_id'],
					'type'		=> 'text',
					'attributes'=> array('onchange' => '$(this).val($(this).val().trim())'),
				);
			}
		}
		
		//------------------------------------------------------------------------------
		// Subscription Products
		//------------------------------------------------------------------------------
		$data['settings'][] = array(
			'key'		=> 'subscription_products',
			'type'		=> 'tab',
		);
		$data['settings'][] = array(
			'type'		=> 'html',
			'content'	=> '<div class="text-info text-center" style="padding-bottom: 5px">' . $data['help_subscription_products'] . '</div>',
		);
		$data['settings'][] = array(
			'key'		=> 'subscription_products',
			'type'		=> 'heading',
		);
		$data['settings'][] = array(
			'key'		=> 'subscriptions',
			'type'		=> 'select',
			'options'	=> array('0' => $data['text_no'], '1' => $data['text_yes']),
		);
		$data['settings'][] = array(
			'key'		=> 'webhook_url',
			'type'		=> 'text',
			'default'	=> str_replace('http:', 'https:', HTTP_CATALOG) . 'index.php?route=' . $this->type . '/' . $this->name . '/webhook&key=' . $this->config->get('config_encryption'),
			'attributes'=> array('readonly' => 'readonly', 'onclick' => 'this.select()', 'style' => 'background: #EEE; cursor: pointer; font-family: monospace; width: 100% !important;'),
			'after'		=> $data['help_webhook_url'],
		);
		$data['settings'][] = array(
			'key'		=> 'prevent_guests',
			'type'		=> 'select',
			'options'	=> array('0' => $data['text_no'], '1' => $data['text_yes']),
		);
		$data['settings'][] = array(
			'key'		=> 'subscription_shipping',
			'type'		=> 'select',
			'options'	=> array('0' => $data['text_no'], '1' => $data['text_yes']),
		);
		
		// Subscription products
		$data['subscription_products'] = array();
		
		if (!empty($data['saved']['server_mode']) &&
			!empty($data['saved'][$data['saved']['server_mode'] . '_merchant_id']) &&
			!empty($data['saved'][$data['saved']['server_mode'] . '_public_key']) &&
			!empty($data['saved'][$data['saved']['server_mode'] . '_private_key'])
		) {
			require_once(DIR_SYSTEM . 'library/braintree/Braintree.php');
			Braintree_Configuration::environment($data['saved']['server_mode']);
			Braintree_Configuration::merchantId($data['saved'][$data['saved']['server_mode'] . '_merchant_id']);
			Braintree_Configuration::publicKey($data['saved'][$data['saved']['server_mode'] . '_public_key']);
			Braintree_Configuration::privateKey($data['saved'][$data['saved']['server_mode'] . '_private_key']);
			
			try {
				$plans = Braintree_Plan::all();
			} catch (Exception $e) {
				$plans = array();
			}
			
			foreach ($plans as $plan) {
				$product_query = $this->db->query("SELECT *, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = " . (int)$this->config->get('config_customer_group_id') . " AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id AND pd.language_id = " . (int)$this->config->get('config_language_id') . ") WHERE p.location = '" . $this->db->escape($plan->_attributes['id']) . "'");
				foreach ($product_query->rows as $product) {
					$data['subscription_products'][] = array(
						'product_id'	=> $product['product_id'],
						'name'			=> $product['name'],
						'price'			=> $this->currency->format($product['special'] ? $product['special'] : $product['price']),
						'location'		=> $product['location'],
						'plan'			=> $plan->_attributes['name'],
						'interval'		=> $plan->_attributes['billingFrequency'] . ($plan->_attributes['billingFrequency'] > 1 ? ' months' : ' month'),
						'charge'		=> $this->currency->format($plan->_attributes['price'])
					);
				}
			}
		}
		
		$subscription_products_table = '
			<table class="table table-stripe table-bordered">
				<thead>
					<tr>
						<td colspan="3" style="text-align: center">' . $data['text_thead_opencart'] . '</td>
						<td colspan="3" style="text-align: center">' . $data['text_thead_braintree'] . '</td>
					</tr>
					<tr>
						<td class="left">' . $data['text_product_name'] . '</td>
						<td class="left">' . $data['text_product_price'] . '</td>
						<td class="left">' . $data['text_location_plan_id'] . '</td>
						<td class="left">' . $data['text_plan_name'] . '</td>
						<td class="left">' . $data['text_plan_interval'] . '</td>
						<td class="left">' . $data['text_plan_charge'] . '</td>
					</tr>
				</thead>
		';
		if (empty($data['subscription_products'])) {
			$subscription_products_table .= '
				<tr><td class="center" colspan="6">' . $data['text_no_subscription_products'] . '</td></tr>
				<tr><td class="center" colspan="6">' . $data['text_create_one_by_entering'] . '</td></tr>
			';
		}
		foreach ($data['subscription_products'] as $product) {
			$highlight = ($product['price'] == $product['charge']) ? '' : 'style="background: #FFD"';
			$subscription_products_table .= '
				<tr>
					<td class="left"><a target="_blank" href="index.php?route=catalog/product/update&amp;product_id=' . $product['product_id'] . '&amp;token=' . $data['token'] . '">' . $product['name'] . '</a></td>
					<td class="left" ' . $highlight . '>' . $product['price'] . '</td>
					<td class="left">' . $product['location'] . '</td>
					<td class="left">' . $product['plan'] . '</td>
					<td class="left">' . $product['interval'] . '</td>
					<td class="left" ' . $highlight . '>' . $product['charge'] . '</td>
				</tr>
			';
		}
		$subscription_products_table .= '</table>';
		
		$data['settings'][] = array(
			'title'		=> str_replace('[server_mode]', ucwords(isset($data['saved']['server_mode']) ? $data['saved']['server_mode'] : 'sandbox'), $data['entry_current_subscriptions']),
			'type'		=> 'html',
			'content'	=> $subscription_products_table,
		);
		
		//------------------------------------------------------------------------------
		// end settings
		//------------------------------------------------------------------------------
		
		$this->document->setTitle($data['heading_title']);
		
		if (version_compare(VERSION, '2.0') < 0) {
			$this->data = $data;
			$this->template = $this->type . '/' . $this->name . '.tpl';
			$this->children = array(
				'common/header',	
				'common/footer',
			);
			$this->response->setOutput($this->render());
		} else {
			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');
			$this->response->setOutput($this->load->view($this->type . '/' . $this->name . '.tpl', $data));
		}
	}
	
	//==============================================================================
	// Setting functions
	//==============================================================================
	private $encryption_key = '';
	private $columns = 7;
	
	private function getTableRowNumbers($data, $table, $sorting) {
		$groups = array();
		$rules = array();
		
		foreach ($data['saved'] as $key => $setting) {
			if (preg_match('/' . $table . '_(\d+)_' . $sorting . '/', $key, $matches)) {
				$groups[$setting][] = $matches[1];
			}
			if (preg_match('/' . $table . '_(\d+)_rule_(\d+)_type/', $key, $matches)) {
				$rules[$matches[1]][] = $matches[2];
			}
		}
		
		if (empty($groups)) {
			$groups = array('' => array('1'));
		}
		ksort($groups, SORT_STRING);
		
		$rows = array();
		foreach ($groups as $group) {
			foreach ($group as $num) {
				$rows[$num] = (empty($rules[$num])) ? array() : $rules[$num];
			}
		}
		
		return $rows;
	}
	
	public function loadSettings(&$data) {
		$backup_type = (empty($data)) ? 'manual' : 'auto';
		if ($backup_type == 'manual' && !$this->user->hasPermission('modify', $this->type . '/' . $this->name)) {
			return;
		}
		
		// Load saved settings
		$data['saved'] = array();
		$settings_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `" . (version_compare(VERSION, '2.0.1') < 0 ? 'group' : 'code') . "` = '" . $this->db->escape($this->name) . "' ORDER BY `key` ASC");
		
		foreach ($settings_query->rows as $setting) {
			$key = str_replace($this->name . '_', '', $setting['key']);
			$value = $setting['value'];
			if ($setting['serialized']) {
				$value = (version_compare(VERSION, '2.1', '<')) ? unserialize($setting['value']) : json_decode($setting['value']);
			}
			
			$data['saved'][$key] = $value;
			
			if (is_array($value)) {
				foreach ($value as $num => $value_array) {
					foreach ($value_array as $k => $v) {
						$data['saved'][$key . '_' . $num . '_' . $k] = $v;
					}
				}
			}
		}
		
		// Load language and check max_input _vars
		$data = array_merge($data, $this->load->language($this->type . '/' . $this->name));
		
		if (ini_get('max_input_vars') && ((ini_get('max_input_vars') - count($data['saved'])) < 50)) {
			$data['warning'] = $data['standard_warning'];
		}
		
		// Set save type
		if (!empty($data['saved']['autosave'])) {
			$data['save_type'] = 'auto';
		}
		
		// Skip auto-backup if not needed
		if ($backup_type == 'auto' && empty($data['autobackup'])) {
			return;
		}
		
		// Create settings auto-backup file
		$manual_filepath = DIR_LOGS . $this->name . '_backup' . $this->encryption_key . '.txt';
		$auto_filepath = DIR_LOGS . $this->name . '_autobackup' . $this->encryption_key . '.txt';
		$filepath = ($backup_type == 'auto') ? $auto_filepath : $manual_filepath;
		if (file_exists($filepath)) unlink($filepath);
		
		if ($this->columns == 3) {
			file_put_contents($filepath, 'EXTENSION	SETTING	VALUE' . "\n", FILE_APPEND|LOCK_EX);
		} elseif ($this->columns == 5) {
			file_put_contents($filepath, 'EXTENSION	SETTING	NUMBER	SUB-SETTING	VALUE' . "\n", FILE_APPEND|LOCK_EX);
		} else {
			file_put_contents($filepath, 'EXTENSION	SETTING	NUMBER	SUB-SETTING	SUB-NUMBER	SUB-SUB-SETTING	VALUE' . "\n", FILE_APPEND|LOCK_EX);
		}
		
		foreach ($data['saved'] as $key => $value) {
			$parts = explode('|', preg_replace(array('/_(\d+)_/', '/_(\d+)/'), array('|$1|', '|$1'), $key));
			
			$line = $this->name . "\t" . $parts[0] . "\t";
			for ($i = 1; $i < $this->columns - 2; $i++) {
				$line .= (isset($parts[$i]) ? $parts[$i] : '') . "\t";
			}
			$line .= str_replace(array("\t", "\n"), array('    ', '\n'), $value) . "\n";
			
			file_put_contents($filepath, $line, FILE_APPEND|LOCK_EX);
		}
		
		$data['autobackup_time'] = date('Y-M-d @ g:i a');
		$data['backup_time'] = (file_exists($manual_filepath)) ? date('Y-M-d @ g:i a', filemtime($manual_filepath)) : '';
		
		if ($backup_type == 'manual') {
			echo $data['autobackup_time'];
		}
	}
	
	public function saveSettings() {
		if (!$this->user->hasPermission('modify', $this->type . '/' . $this->name)) {
			echo 'PermissionError';
			return;
		}
		
		if ($this->request->get['saving'] == 'manual') {
			$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `" . (version_compare(VERSION, '2.0.1') < 0 ? 'group' : 'code') . "` = '" . $this->db->escape($this->name) . "' AND `key` != '" . $this->db->escape($this->name . '_module') . "'");
		}
		
		$modules = array();
		foreach ($this->request->post as $key => $value) {
			if (strpos($key, 'module_') === 0) {
				$parts = explode('_', $key, 3);
				$modules[$parts[1]][$parts[2]] = $value;
			} else {
				if ($this->request->get['saving'] == 'auto') {
					$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `" . (version_compare(VERSION, '2.0.1') < 0 ? 'group' : 'code') . "` = '" . $this->db->escape($this->name) . "' AND `key` = '" . $this->db->escape($this->name . '_' . $key) . "'");
				}
				$this->db->query("
					INSERT INTO " . DB_PREFIX . "setting SET
					`store_id` = 0,
					`" . (version_compare(VERSION, '2.0.1') < 0 ? 'group' : 'code') . "` = '" . $this->db->escape($this->name) . "',
					`key` = '" . $this->db->escape($this->name . '_' . $key) . "',
					`value` = '" . $this->db->escape(stripslashes(is_array($value) ? implode(';', $value) : $value)) . "',
					`serialized` = 0
				");
			}
		}
		
		if (version_compare(VERSION, '2.0.1') < 0) {
			$module_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `" . (version_compare(VERSION, '2.0.1') < 0 ? 'group' : 'code') . "` = '" . $this->db->escape($this->name) . "'AND `key` = '" . $this->db->escape($this->name . '_module') . "'");
			if ($module_query->num_rows) {
				foreach (unserialize($module_query->row['value']) as $key => $value) {
					foreach ($value as $k => $v) {
						if (!isset($modules[$key][$k])) $modules[$key][$k] = $v;
					}
				}
			}
			
			if (isset($modules[0])) {
				$index = 1;
				while (isset($modules[$index])) {
					$index++;
				}
				$modules[$index] = $modules[0];
				unset($modules[0]);
			}
			
			$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `" . (version_compare(VERSION, '2.0.1') < 0 ? 'group' : 'code') . "` = '" . $this->db->escape($this->name) . "'AND `key` = '" . $this->db->escape($this->name . '_module') . "'");
			$this->db->query("
				INSERT INTO " . DB_PREFIX . "setting SET
				`store_id` = 0,
				`" . (version_compare(VERSION, '2.0.1') < 0 ? 'group' : 'code') . "` = '" . $this->db->escape($this->name) . "',
				`key` = '" . $this->db->escape($this->name . '_module') . "',
				`value` = '" . $this->db->escape(serialize($modules)) . "',
				`serialized` = 1
			");
		} else {
			foreach ($modules as $module_id => $module) {
				$module_settings = (version_compare(VERSION, '2.1', '<')) ? serialize($module) : json_encode($module);
				if ($module_id) {
					$this->db->query("
						UPDATE " . DB_PREFIX . "module SET
						`name` = '" . $this->db->escape($module['name']) . "',
						`code` = '" . $this->db->escape($this->name) . "',
						`setting` = '" . $this->db->escape($module_settings) . "'
						WHERE module_id = " . (int)$module_id . "
					");
				} else {
					$this->db->query("
						INSERT INTO " . DB_PREFIX . "module SET
						`name` = '" . $this->db->escape($module['name']) . "',
						`code` = '" . $this->db->escape($this->name) . "',
						`setting` = '" . $this->db->escape($module_settings) . "'
					");
				}
			}
		}
	}
	
	public function deleteSetting() {
		$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `" . (version_compare(VERSION, '2.0.1') < 0 ? 'group' : 'code') . "` = '" . $this->db->escape($this->name) . "' AND `key` = '" . $this->db->escape($this->name . '_' . str_replace('[]', '', $this->request->get['setting'])) . "'");
	}
	
	//==============================================================================
	// Custom functions
	//==============================================================================
	public function submit() {
		$data = array('autobackup' => false);
		$this->loadSettings($data);
		
		require_once(DIR_SYSTEM . 'library/braintree/Braintree.php');
		Braintree_Configuration::environment($data['saved']['server_mode']);
		Braintree_Configuration::merchantId($data['saved'][$data['saved']['server_mode'] . '_merchant_id']);
		Braintree_Configuration::publicKey($data['saved'][$data['saved']['server_mode'] . '_public_key']);
		Braintree_Configuration::privateKey($data['saved'][$data['saved']['server_mode'] . '_private_key']);
		
		$result = Braintree_Transaction::submitForSettlement($this->request->get['charge_id']);
		if (!$result->success) {
			$this->log->write(strtoupper($this->name) . ' SUBMIT ERROR: ' . $result->message);
			echo 'Error: ' . $result->message;
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "order_history SET `comment` = REPLACE(`comment`, '<span>No &nbsp;</span> <a style=\"cursor: pointer\" onclick=\"braintreeSubmit($(this), \'" . $this->db->escape($this->request->get['charge_id']) . "\')\">(Submit Now)</a>', 'Yes') WHERE `comment` LIKE '%submit($(this), \'" . $this->db->escape($this->request->get['charge_id']) . "\')%'");
		}
	}
	
	public function refund() {
		$data = array('autobackup' => false);
		$this->loadSettings($data);
		
		require_once(DIR_SYSTEM . 'library/braintree/Braintree.php');
		Braintree_Configuration::environment($data['saved']['server_mode']);
		Braintree_Configuration::merchantId($data['saved'][$data['saved']['server_mode'] . '_merchant_id']);
		Braintree_Configuration::publicKey($data['saved'][$data['saved']['server_mode'] . '_public_key']);
		Braintree_Configuration::privateKey($data['saved'][$data['saved']['server_mode'] . '_private_key']);
		
		$result = Braintree_Transaction::refund($this->request->get['charge_id']);
		if (!$result->success) {
			$result = Braintree_Transaction::void($this->request->get['charge_id']);
			if (!$result->success) {
				$this->log->write(strtoupper($this->name) . ' REFUND ERROR: ' . $result->message);
				echo 'Error: ' . $result->message;
			}
		}
	}
}
?>