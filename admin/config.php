<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/sites/working-chairs/admin/');
define('HTTP_CATALOG', 'http://localhost/sites/working-chairs/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/sites/working-chairs/admin/');
define('HTTPS_CATALOG', 'http://localhost/sites/working-chairs/');

// DIR
define('DIR_APPLICATION', 'C:\xampp\htdocs\sites\working-chairs/admin/');
define('DIR_SYSTEM', 'C:\xampp\htdocs\sites\working-chairs/system/');
define('DIR_DATABASE', 'C:\xampp\htdocs\sites\working-chairs/system/database/');
define('DIR_LANGUAGE', 'C:\xampp\htdocs\sites\working-chairs/admin/language/');
define('DIR_TEMPLATE', 'C:\xampp\htdocs\sites\working-chairs/admin/view/template/');
define('DIR_CONFIG', 'C:\xampp\htdocs\sites\working-chairs/system/config/');
define('DIR_IMAGE', 'C:\xampp\htdocs\sites\working-chairs/image/');
define('DIR_CACHE', 'C:\xampp\htdocs\sites\working-chairs/system/cache/');
define('DIR_DOWNLOAD', 'C:\xampp\htdocs\sites\working-chairs/download/');
define('DIR_LOGS', 'C:\xampp\htdocs\sites\working-chairs/system/logs/');
define('DIR_CATALOG', 'C:\xampp\htdocs\sites\working-chairs/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'wcnew_news');
define('DB_PASSWORD', 'eLky748snBmv');
define('DB_DATABASE', 'wcnew_news');
define('DB_PREFIX', 'oc_');
?>